import { Base } from './Base.model';

export class User extends Base {
  email?: string;
  first_name: string;
  last_name: string;
  image: string; // url
  role: number;
  is_active: boolean;
  classrooms_count: number;
}

export enum UserRole {
  STAFF = 1,
  TEACHER = 2,
  STUDENT = 3,
}
