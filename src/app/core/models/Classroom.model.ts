import { Base } from './Base.model';
import { User } from './User.model';
import { Image } from './Image.model';
import { TinusaurShield } from './api-request/project-api-request';

export class Classroom extends Base {
  owner?: User;
  owner_id: number | string;
  members: Array<User | string>;
  title: string;
  description: string;
  finished: boolean;
  images: Array<Image>
  shield?: TinusaurShield;
  created_at: string;
  updated_at: string;

  is_new_classroom_button_holder?: boolean;
}
