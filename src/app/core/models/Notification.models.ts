import { Base } from './Base.model';

export class Notification extends Base {
  owner: string;
  owner_id: number;
  recipient: string;
  recipient_id: number;
  title: string;
  type: 'success' | 'error' | 'warning' | 'info';
  text: string;
  notification_datetime: string;
  notified: boolean;
  redirect_path: string;
}
