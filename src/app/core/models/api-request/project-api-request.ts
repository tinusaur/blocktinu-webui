import { UploadFile } from 'ng-zorro-antd';

export enum TinusaurShield {
  ledx2 = 'ledx2',
  ledx3 = 'ledx3',
  edux3io = 'edux3io',
  magx3io = 'magx3io',
  kidsmicro = 'kidsmicro',
  kidsmini = 'kidsmini',
}

export enum TinusaurShieldDescription {
  ledx2 = 'Shield LEDx2',
  ledx3 = 'Shield LEDx3',
  edux3io = 'Shield EDUx3IO',
  magx3io = 'Shield MAGx3IO',
  kidsmicro = 'Board KidsMicro',
  kidsmini = 'Board KidsMini',
}

export const DEFAULT_TINUSAUR_SHIELD = TinusaurShield.ledx2;

export class ProjectApiRequest {
  constructor(
    public title: string,
    public description: string,
    public type: 'block' | 'code',
    public shield: TinusaurShield,
    public images: Array<UploadFile>,
    public tags: Array<string>,
    public classroom?: string, // url
    public code?: string
  ) { }
}
