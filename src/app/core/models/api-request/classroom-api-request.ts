import { UploadFile } from 'ng-zorro-antd';

export class ClassroomApiRequest {
  constructor(
    public title: string,
    public description: string,
    public members: Array<string>, // urls
    public images?: Array<UploadFile>,
  ) { }
}
