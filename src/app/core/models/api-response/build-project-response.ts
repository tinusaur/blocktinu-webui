export class BuildProjectResponse {
  success: boolean;
  message: string;

  header_offset?: number;
  footer_offset?: number;
  bthex?: string;
  build_log: string;
  build_err: string;
}
