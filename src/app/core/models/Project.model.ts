import { Base } from './Base.model';
import { User } from './User.model';
import { Image } from './Image.model';
import { Tag } from './Tag.models';
import { TinusaurShield } from './api-request/project-api-request';

export class Project extends Base {
  owner?: User | string;
  owner_id: number;
  classroom?: string; // url
  classroom_id?: number;
  classroom_name?: string;
  owner_name?: string;
  title: string;
  description: string;
  is_private: boolean;
  finished: boolean;
  autosave: boolean;
  type: 'block' | 'code';
  images: Array<Image>;
  tags: Array<Tag>;
  code?: string; // TODO
  libraries: Array<string> | null;
  shield: TinusaurShield;

  created_at: string;
  updated_at: string;

  is_demo?: boolean;
  is_new_project_button_holder?: boolean;
}
