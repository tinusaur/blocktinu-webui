/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SocketWrapperService } from './socket-wrapper.service';

describe('Service: SocketWrapper', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocketWrapperService]
    });
  });

  it('should ...', inject([SocketWrapperService], (service: SocketWrapperService) => {
    expect(service).toBeTruthy();
  }));
});
