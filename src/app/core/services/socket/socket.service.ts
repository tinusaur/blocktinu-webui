import { Injectable, OnDestroy, OnInit, PLATFORM_ID, Inject } from "@angular/core";
import { Observable, Observer, Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { share } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';
import { isPlatformBrowser } from '@angular/common';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';

const RECONNECTION_INTERVAL = 12000;

@Injectable({
  providedIn: 'root'
})
export class SocketService implements OnDestroy, OnInit {

  // holder for nzMessageService msg id's
  private nzLoadingMsgId = null;
  private reconnect$ = new BehaviorSubject<Subject<MessageEvent>>(null);
  private subject: Subject<MessageEvent>;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private credentialsService: CredentialsService,
    private nzMessageService: NzMessageService,
  ) { }

  ngOnInit() { }

  ngOnDestroy() {
    if (this.subject) this.subject.complete();
  }

  // all service clients can listen for reconnection event.
  public onReconnectObservable() {
    return this.reconnect$.asObservable().pipe(share());
  }

  /*
   * Connect to socket url
   * force = true means that it will reconnect even if it's connected to the same url
   */
  public connect(force = false): Subject<MessageEvent> {
    if (this.subject && force) {
      this.subject.complete();
      this.subject = null;
      this.reconnect$.next(null);
    }

    if (!this.subject) {
      const url = this.getSocketUrl();
      this.subject = this.create(url);
    }

    // this.reconnect$.next(this.subject);
    return this.subject;
  }

  private getSocketUrl(): string {
    return `${environment.WEBSOCKET_PROTOCOL}://${environment.WEBSOCKET_URL}/global/`
  }

  /*
   * Create a new WS and a Subject of MessageEvents
   * Used by connect() method
   */
  private create(url): Subject<MessageEvent> {
    if (!isPlatformBrowser(this.platformId)) {
      // we're not on a regular browser, pass.
      return null;
    }

    // get token
    const token = this.credentialsService.getTokenInfo();
    if (!token || !this.credentialsService.isLoggedIn) {
      // console.warn("Cannot create secured socket for anonymous user.");
      return;
    }

    // prepare a reconnect function
    const that = this;
    const reconnect = (e) => {
      if (this.nzLoadingMsgId === null) {
        this.nzLoadingMsgId = this.nzMessageService.loading('Connection lost. Reconnecting ...', { nzDuration: 1000 }).messageId;
      }
	  else { this.nzLoadingMsgId = this.nzMessageService.loading('Connected', { nzDuration: 1000 }).messageId; }
	  
      console.warn(`Socket is closed. Reconnect will be attempted in ${RECONNECTION_INTERVAL / 1000} second(s).`, e.reason);
      setTimeout(() => {
        that.connect(true);
      }, RECONNECTION_INTERVAL);
    }

    // try to open ws passing authorization token in a safe manner
    // see: https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers

    const protocol = token.auth_token;
    let ws = new WebSocket(url, protocol);
    // reconnect on close
    ws.onclose = reconnect;
    // log on open
    ws.onopen = (e) => {
      if (ws.OPEN) {
        if (this.nzLoadingMsgId) {
          this.nzMessageService.remove(this.nzLoadingMsgId);
          this.nzLoadingMsgId = null;
        }
        console.log("Successfully connected: " + url);
        that.reconnect$.next(that.subject);
      } else {
        console.log("Failure while connecting: " + url);
	  }
    }

    // setup subject and bind it
    let observable = Observable.create((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = (e) => {
        obs.complete.call(obs);
        reconnect(e);
      }
      return ws.close.bind(ws);
    });

    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      },
      complete: () => {
        ws.close();
        this.subject = null;
      }
    };
    return Subject.create(observer, observable);
  }
}
