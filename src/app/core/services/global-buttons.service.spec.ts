/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GlobalButtonsService } from './global-buttons.service';

describe('Service: GlobalButtons', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalButtonsService]
    });
  });

  it('should ...', inject([GlobalButtonsService], (service: GlobalButtonsService) => {
    expect(service).toBeTruthy();
  }));
});
