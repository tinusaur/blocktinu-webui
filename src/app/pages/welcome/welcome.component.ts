import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(
    private router: Router,
    private credentialsService: CredentialsService
  ) { }

  ngOnInit() {
    if (this.credentialsService.isLoggedIn) {
      this.router.navigate(['/', 'projects', 'my']);
    } else {
      this.router.navigate(['/', 'projects', 'details', 'block-demo']);
    }
  }

}
