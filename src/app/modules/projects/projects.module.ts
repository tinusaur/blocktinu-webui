import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectsDetailComponent } from './components/projects-detail/projects-detail.component';
import { ProjectsRouting } from './projects-routing.module';
import {
  NzMenuModule, NzInputModule, NzGridModule, NzCardModule,
  NzButtonModule, NzIconModule, NzDividerModule, NzListModule,
  NzTagModule, NzToolTipModule, NzBadgeModule, NzModalModule,
  NzSwitchModule, NzDescriptionsModule, NzDrawerModule, NzTypographyModule, NzCheckboxModule, NzSelectModule, NzPaginationModule, NzFormModule, NzPopconfirmModule, NzResultModule, NzRadioModule
} from 'ng-zorro-antd';
import { ProjectsExploreComponent } from './components/projects-explore/projects-explore.component';
import { ProjectsListPageComponent } from './components/projects-list-page/projects-list-page.component';
import { MyProjectsComponent } from './components/my-projects/my-projects.component';
import { ProjectsFiltersComponent } from './components/projects-filters/projects-filters.component';
import { TagsSelectorModule } from '../shared/tags-selector/tags-selector.module';
import { ProjectsListItemComponent } from './components/projects-list-item/projects-list-item.component';
import { CodeEditorModule } from '../shared/code-editor/code-editor.module';
import { BlocklyEditorModule } from '../shared/blockly-editor/blockly-editor.module';
import { NzResizableModule } from 'ng-zorro-antd/resizable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TerminalModule } from '../shared/terminal/terminal.module';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ImagesSelectorModule } from '../shared/images-selector/images-selector.module';
import { ClassroomSelectorModule } from '../shared/classroom-selector/classroom-selector.module';
import { ProjectsDemoComponent } from './components/projects-demo/projects-demo.component';
import { MigrateDemoProjectsComponent } from './components/migrate-demo-projects/migrate-demo-projects.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ProjectsRouting,
    NzMenuModule,
    NzInputModule,
    NzRadioModule,
    NzGridModule,
    NzCardModule,
    NzButtonModule,
    NzIconModule,
    NzDividerModule,
    NzListModule,
    NzResizableModule,
    NzTagModule,
    TagsSelectorModule,
    ClassroomSelectorModule,
    CodeEditorModule,
    TerminalModule,
    BlocklyEditorModule,
    ImagesSelectorModule,
    NzToolTipModule,
    NzBadgeModule,
    NzModalModule,
    NzDrawerModule,
    NzTypographyModule,
    NzCheckboxModule,
    NzSelectModule,
    NzDescriptionsModule,
    NzSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    NzFormModule,
    NzPaginationModule,
    NzPopconfirmModule,
    NzResultModule
  ],
  declarations: [
    ProjectsListPageComponent,
    MyProjectsComponent,
    ProjectsListComponent,
    ProjectsListItemComponent,
    ProjectsExploreComponent,
    ProjectsDetailComponent,
    CreateProjectComponent,
    ProjectsFiltersComponent,
    ProjectsDemoComponent,
    MigrateDemoProjectsComponent
  ],
  exports: [
    ProjectsListComponent
  ]
})
export class ProjectsModule { }
