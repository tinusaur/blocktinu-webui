import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListPageComponent } from './components/projects-list-page/projects-list-page.component';
import { ProjectsExploreComponent } from './components/projects-explore/projects-explore.component';
import { MyProjectsComponent } from './components/my-projects/my-projects.component';
import { ProjectsDetailComponent } from './components/projects-detail/projects-detail.component';
import { ProjectsResolverService } from './services/projects-resolver.service';
import { ProjectDetailsResolverService } from './services/project-details-resolver.service';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { AuthGuard } from '../auth/auth.guard';
import { ProjectsDemoComponent } from './components/projects-demo/projects-demo.component';
import { PendingChangesGuard } from 'src/app/core/services/guards/pending-changes.guard';
import { MigrateDemoProjectsComponent } from './components/migrate-demo-projects/migrate-demo-projects.component';

const routes: Routes = [
  {
    path: 'create',
    component: CreateProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'details/:projectId',
    component: ProjectsDetailComponent,
    resolve: {
      project: ProjectDetailsResolverService,
    },
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    canDeactivate: [PendingChangesGuard]
  },
  {
    path: 'migrate-demo',
    component: MigrateDemoProjectsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: ProjectsListPageComponent,
    children: [
      { path: '', pathMatch: 'exact', redirectTo: 'my' },
      {
        path: 'my',
        component: MyProjectsComponent,
        resolve: {
          projects: ProjectsResolverService,
        },
        data: {
          only_current_user_projects: true
        },
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        canActivate: [AuthGuard]
      },
      {
        path: 'explore',
        component: ProjectsExploreComponent,
        resolve: {
          projects: ProjectsResolverService,
        },
        data: {},
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
      },
      {
        path: 'demo',
        component: ProjectsDemoComponent,
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRouting { }
