import { Component, OnInit } from '@angular/core';
import { DemoProjectsService } from '../../services/demo-projects.service';
import { Project } from 'src/app/core/models/Project.model';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ProjectsService } from '../../services/projects.service';
import { ProjectApiRequest } from 'src/app/core/models/api-request/project-api-request';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-migrate-demo-projects',
  templateUrl: './migrate-demo-projects.component.html',
  styleUrls: ['./migrate-demo-projects.component.scss']
})
export class MigrateDemoProjectsComponent implements OnInit {

  // demo projects array
  demoProjects: Array<Project> = new Array<Project>();
  // current project index in demoProjects Array
  index = 0;
  // ?
  showModal: boolean = false;
  // current project for migration
  selectedProject: Project;
  // saving flag
  saving = false;
  // use ngmodel to get title input from user
  title: string;

  constructor(
    private demoProjectsService: DemoProjectsService,
    private router: Router,
    private nzMessageService: NzMessageService,
    private projectsService: ProjectsService,
  ) {
  }

  ngOnInit() {
    // get demo projects
    this.demoProjects = this.demoProjectsService.getDemoProjects().filter(p => p.code && p.code.length > 1);
    if (this.demoProjects.length > 0) {
      // there is at least one project, let's try to migrate it.
      this.index = 0;
      this.selectedProject = this.demoProjects[this.index];
      this.showModal = true;
    } else {
      // no demo projects
      this.router.navigate(['/', 'projects']);
    }
  }

  // save project
  saveProject() {
    if (!this.title) {
      // errr
      this.nzMessageService.error("Provide a title for new project");
      return;
    }
    if (!this.saving) {
      this.saving = true;

      const projectReq: ProjectApiRequest = {
        title: this.title,
        description: this.selectedProject.description,
        type: this.selectedProject.type,
        shield: this.selectedProject.shield,
        images: [],
        tags: this.selectedProject.tags.map(v => v.name),
        code: this.selectedProject.code
      };

      this.projectsService.create(projectReq)
        .subscribe(event => {
          if (event instanceof HttpResponse) {
            // created
            const newProject = event.body;
            const id = newProject['id'];
            this.nzMessageService.success(`Successfully migrated demo project to project # ${id}`);
            this.saving = false;
            this.title = null;
            // go to the next demo project
            this.next();
          }
        }, error => {
          this.nzMessageService.error("Error migrating project");
        });
    }
  }

  // Move on to the next project
  next() {
    if (this.selectedProject) {
      this.demoProjectsService.removeDemoProject(this.selectedProject);
    }
    this.showModal = false;
    this.index++;
    if (this.demoProjects.length < this.index + 1) {
      this.router.navigate(['/', 'projects'])
    } else {
      this.selectedProject = this.demoProjects[this.index];
      setTimeout(() => this.showModal = true, 100);
    }
  }
}
