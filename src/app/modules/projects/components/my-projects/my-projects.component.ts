import { Component, OnInit } from '@angular/core';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Project } from 'src/app/core/models/Project.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.scss']
})
export class MyProjectsComponent implements OnInit {

  paginatedProjects: PaginatedApiResponse<Project> = new PaginatedApiResponse<Project>();
  page;
  page_size;
  loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public tds: TargetDeviceService
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(params => {
      this.page_size = +params.get('page_size') || this.page_size;
      this.page = +params.get('page') || this.page;
    });

    // unsubscription is handled by ActivatedRoute
    this.route.data
      .subscribe((data: { projects: PaginatedApiResponse<Project> }) => {
        if (data.projects) {
          this.paginatedProjects = data.projects;
          this.loading = false;
        }
      });
  }

  search(q: string) {
    this.loading = true;
    const queryParams: Params = { q };
    this.updateQueryParams(queryParams);
  }

  loadDataForPage(pi: number): void {
    this.loading = true;
    this.page = pi;
    const queryParams: Params = { page: pi, page_size: this.page_size };
    this.updateQueryParams(queryParams);
  }

  private updateQueryParams(queryParams: Params) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
        queryParamsHandling: "merge", // remove to replace all query params by provided
      });
  }


}
