import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projects-filters',
  templateUrl: './projects-filters.component.html',
  styleUrls: ['./projects-filters.component.scss']
})
export class ProjectsFiltersComponent implements OnInit {

  @Output() onSearchQueryChange = new EventEmitter<string>();
  searchValue: string;
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // not unsubscribing, it's ok. ActivatedRoute will handle this subscription
    this.route.queryParamMap.subscribe(qParamMap => {
      const q = qParamMap.get('q');
      if (q) this.searchValue = q;
    });
  }

  emitNewSearch() {
    this.onSearchQueryChange.emit(this.searchValue);
  }

}
