import { Component, OnInit } from '@angular/core';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Project } from 'src/app/core/models/Project.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';

@Component({
  selector: 'app-projects-explore',
  templateUrl: './projects-explore.component.html',
  styleUrls: ['./projects-explore.component.scss']
})
export class ProjectsExploreComponent implements OnInit {
  paginatedProjects: PaginatedApiResponse<Project> = new PaginatedApiResponse<Project>();
  page;
  page_size;
  loading: boolean = false;
  showTrending: boolean = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public tds: TargetDeviceService
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(params => {
      this.page_size = +params.get('page_size') || this.page_size;
      this.page = +params.get('page') || this.page;
    });

    // unsubscription is handled by ActivatedRoute
    this.route.data
      .subscribe((data: { projects: PaginatedApiResponse<Project>, hide_trending: boolean }) => {
        if (data.projects) {
          this.paginatedProjects = data.projects;
          this.loading = false;
        }
        if ('hide_trending' in data && data.hide_trending === true) {
          this.showTrending = false;
        }
      });
  }

  search(q: string) {
    this.loading = true;
    const queryParams: Params = { q };
    this.updateQueryParams(queryParams);
  }

  loadDataForPage(pi: number): void {
    this.loading = true;
    this.page = pi;
    const queryParams: Params = { page: pi, page_size: this.page_size };
    this.updateQueryParams(queryParams);
  }

  private updateQueryParams(queryParams: Params) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
        queryParamsHandling: "merge", // remove to replace all query params by provided
      });
  }
}
