import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Project } from 'src/app/core/models/Project.model';
import { Classroom } from 'src/app/core/models/Classroom.model';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit, OnChanges {

  @Input() classroom: Classroom = null;
  @Input() projects: PaginatedApiResponse<Project>;
  @Input() showCreateNewProjectButton: boolean = true;
  @Input() loading: boolean = false;
  @Output() onPageIndexChange = new EventEmitter<number>();
  emptyProjectListHolder = new Project();
  constructor() {
    this.emptyProjectListHolder.title = "Create New Project";
    this.emptyProjectListHolder.is_new_project_button_holder = true;
  }

  ngOnInit() {
  }

  onPageChange(newPageIndex: number) {
    this.onPageIndexChange.emit(newPageIndex);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.showCreateNewProjectButton &&
      changes['projects'] &&
      changes.projects.previousValue != changes.projects.currentValue) {
      this.projects.results.unshift(this.emptyProjectListHolder);
    }
  }
}
