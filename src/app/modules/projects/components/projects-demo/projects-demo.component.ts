import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/core/models/Project.model';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { DemoProjectsService } from '../../services/demo-projects.service';

@Component({
  selector: 'app-projects-demo',
  templateUrl: './projects-demo.component.html',
  styleUrls: ['./projects-demo.component.scss']
})
export class ProjectsDemoComponent implements OnInit {

  demoProjects: PaginatedApiResponse<Project> = new PaginatedApiResponse<Project>();

  constructor(
    public tds: TargetDeviceService,
    private demoProjectsService: DemoProjectsService
  ) {
  }

  ngOnInit() {
    this.demoProjects.results = this.demoProjectsService.getDemoProjects();
  }
}
