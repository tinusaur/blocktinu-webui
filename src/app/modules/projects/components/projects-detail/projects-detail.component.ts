import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, filter } from 'rxjs/operators';
import { Project } from 'src/app/core/models/Project.model';
import { Observable, of, Subscription } from 'rxjs';
import { BlocklyEditorComponent } from 'src/app/modules/shared/blockly-editor/blockly-editor.component';
import { ProjectDetailService } from './project-detail.service';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';
import { User } from 'src/app/core/models/User.model';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { TerminalComponent } from 'src/app/modules/shared/terminal/terminal.component';
import { BuildProjectResponse } from 'src/app/core/models/api-response/build-project-response';
import { NzMessageService } from 'ng-zorro-antd';
import { CodeEditorComponent, Marker } from 'src/app/modules/shared/code-editor/code-editor.component';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/core/services/user.service';
import { ProjectSocketSharingService } from './project-socket-sharing.service';
import { ComponentCanDeactivate } from 'src/app/core/services/guards/pending-changes.guard';
import { TinusaurShield, TinusaurShieldDescription } from 'src/app/core/models/api-request/project-api-request';
import { GlobalButtonsService } from 'src/app/core/services/global-buttons.service';

const INITIAL_COL = 16;

@Component({
  selector: 'app-projects-detail',
  templateUrl: './projects-detail.component.html',
  styleUrls: ['./projects-detail.component.scss'],
  providers: [ProjectDetailService, ProjectSocketSharingService]
})
export class ProjectsDetailComponent implements OnInit, AfterViewInit, OnDestroy, ComponentCanDeactivate {
  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    // insert logic to check if there are pending changes here;
    // returning true will navigate without confirmation
    // returning false will show a confirm dialog before navigating away
    return !this.projectDetailService.unsaved && !this.projectDetailService.building && !this.projectDetailService.saving;
  }

  @HostListener('document:keydown.control.b', ['$event']) onKeydownHandlerCtrlB(event: KeyboardEvent) {
    this.handleCommand('build');
  }

  @HostListener('document:keydown.control.s', ['$event']) onKeydownHandlerCtrlS(event: KeyboardEvent) {
    this.handleCommand('save');
    event.preventDefault();
  }

  shields = Object.entries(TinusaurShield).map(e => ({ key: e[0], value: TinusaurShieldDescription[e[0]] }));  // shields = Object.keys(TinusaurShield);

  public project$: Observable<Project>;
  public owner: User;
  public libraries = [
    { label: "TinyAVRLib ADCxLib", value: "tinyavrlib-adcxlib", checked: false },
    { label: "TinyAVRLib Scheduler", value: "tinyavrlib-scheduler", checked: false },
  ]

  @ViewChild(TerminalComponent, { static: false }) terminal: TerminalComponent;
  @ViewChild(CodeEditorComponent, { static: false }) codeEditor: CodeEditorComponent;
  @ViewChild(BlocklyEditorComponent, { static: false }) blocklyEditor: BlocklyEditorComponent;

  col = INITIAL_COL;
  private animationFrameId = -1;

  expandedMenu: boolean = false;

  showSettingsModal: boolean = false;
  showUnsavedAttensionAnimation: boolean = true;
  unsavedLibraries: boolean = false;

  ngAfterViewInit() {
    if (this.blocklyEditor) this.blocklyEditor.resize();
  }

  constructor(
    public credentialsService: CredentialsService,
    private route: ActivatedRoute,
    public projectDetailService: ProjectDetailService,
    public tds: TargetDeviceService,
    private nzMessageService: NzMessageService,
    private userService: UserService,
    private globalButtonsService: GlobalButtonsService
  ) { }

  unfoldMenu() {
    if (this.tds.isMobile) {
      this.expandedMenu = true;
    }
  }

  foldMenu() {
    if (this.tds.isMobile) {
      this.expandedMenu = false;
    }
  }

  private projectSubscription: Subscription;
  private globalBuildButtonSub: Subscription;
  private globalShieldsButtonSub: Subscription;
  ngOnDestroy() {
    if (this.projectSubscription) this.projectSubscription.unsubscribe();
    if (this.globalBuildButtonSub) this.globalBuildButtonSub.unsubscribe();
    if (this.globalShieldsButtonSub) this.globalShieldsButtonSub.unsubscribe();
    // hide global buttons
    this.globalButtonsService.showButtons = false;
  }

  showShieldsModal: boolean = false;

  openShieldsModal(isOpen: boolean) {
    this.showShieldsModal = isOpen;
    if (!isOpen) {
      // TODO: you can delete this one
      this.projectDetailService.saveCode(); // Save the code after the shield has been changed, i.e. modal is closed.
    }
  }

  async ngOnInit() {
    // show global buttons
    this.globalButtonsService.showButtons = true;
    // subscribe for global buttons
    this.globalBuildButtonSub = this.globalButtonsService.getOnBuildButtonPressedObservable().pipe(filter(v => v == true)).subscribe(v => this.handleCommand('build'));
    this.globalShieldsButtonSub = this.globalButtonsService.getOnShieldsButtonPressedObservable().pipe(filter(v => v == true)).subscribe(v => this.openShieldsModal(true));

    // create project observable
    this.project$ = this.route.data.pipe(
      map((data: { project: Project }) => {
        // fillup Empty Block Project
        if (data.project.code === "" && data.project.type === 'block') {
          data.project.code = `<xml xmlns="https://developers.google.com/blockly/xml"/>`
        }
        return data.project;
      })
    );

    // subscribe for project observable
    this.projectSubscription = this.project$.subscribe(v => {
      this.projectDetailService.setProject(v);
      if (this.projectDetailService.project.type == 'block') {
        this.hideCodeEditor();
      }

      if (this.projectDetailService.project.is_demo) {
        this.projectDetailService.readonly = false;
      } else if (!this.owner) {
        if (this.credentialsService.currentUser &&
          v.owner_id == this.credentialsService.currentUser.id) {
          this.owner = this.credentialsService.currentUser;
          this.projectDetailService.readonly = false;
        } else {
          // unsub is handled by UserService
          this.userService.getUserById(v.owner_id)
            .subscribe(owner => this.owner = owner);
          this.projectDetailService.readonly = true;
        }
      } else {
        this.projectDetailService.readonly = !this.credentialsService.currentUser || this.projectDetailService.project.owner_id != this.credentialsService.currentUser.id;
      }
      this.libraries = this.libraries.map(libraryDescription => {
        return { ...libraryDescription, checked: !v.libraries || v.libraries.includes(libraryDescription.value) }
      });
    });
  }

  public onBlocklyC11Code(code: string) {
    this.projectDetailService.setC11(code);
  }

  public onBlocklyXml(xml: string) {
    this.projectDetailService.setXml(xml);
  }

  public onCodeEditorChanges(code: string) {
    this.projectDetailService.setC11(code);
  }

  public onResize({ col }: { col: number }): void {
    cancelAnimationFrame(this.animationFrameId);
    this.animationFrameId = requestAnimationFrame(() => {
      this.col = col;
      if (this.blocklyEditor) {
        this.blocklyEditor.resize();
      }
    });
  }

  public toggleCodeEditor() {
    if (this.col === 24) {
      this.onResize({ col: INITIAL_COL })
    } else {
      this.onResize({ col: 24 })
    }
  }

  public showCodeEditor() {
    if (this.col === 24) {
      this.onResize({ col: INITIAL_COL })
    }
  }

  public hideCodeEditor() {
    if (this.col !== 24) {
      this.onResize({ col: 24 })
    }
  }

  public onLibrariesChange(newLibraries: Array<string>) {
    console.log(newLibraries);
    this.unsavedLibraries = true;
  }

  public saveLibraries() {
    console.log('saving libraries');
    this.unsavedLibraries = false;
  }

  handleCommand(command: string) {
    switch (command) {
      case 'download':
        this.projectDetailService.download();
        break;
      case 'save':
        this.projectDetailService.saveCode();
        break;
      case 'build':
        this.projectDetailService.saveCode();
        this.projectDetailService.build()
          .subscribe((response: BuildProjectResponse) => {
            this.processBuildProjectResponse(response, true);
          }, e => {
            this.terminal.error("Failed to connect to build server");
            this.terminal.resume();
          });
        break;
      case 'compile':
        this.projectDetailService.saveCode();
        this.projectDetailService.build()
          .subscribe((response: BuildProjectResponse) => {
            this.processBuildProjectResponse(response, false);
          }, e => {
            this.terminal.error("Failed to connect to build server");
            this.terminal.resume();
          });
        break;
      default:
        break;
    }
  }

  private processBuildProjectResponse(response: BuildProjectResponse, downloadBthex: boolean) {
    const pre = "--------------- \r\n";

    if (response.build_err) response.build_err = atob(response.build_err).replace(/\r?\n/g, "\r\n");
    if (response.build_log) response.build_log = atob(response.build_log).replace(/\r?\n/g, "\r\n");
    this.projectDetailService.setNewBthexContent(null);
    if (response.bthex) {
      response.bthex = atob(response.bthex).replace(/\r?\n/g, "\r\n");
      this.projectDetailService.setNewBthexContent(response.bthex);
      if (downloadBthex) this.handleCommand('download');
    }

    this.terminal.block();

    this.terminal.echo('\n');
    if (response.build_log) {
      this.terminal.echo(this.terminal.colorizeHeader(pre + "Build Log:\r\n" + pre) + response.build_log);
    }

    if (response.build_err) {
      if (response.success) {
        this.terminal.echo(this.terminal.colorizeHeader(pre + "Build Warings:\r\n" + pre) + response.build_err)
      } else {
        this.terminal.echo(this.terminal.colorizeHeader(pre + "Build Errors:\r\n" + pre));
        this.terminal.error(response.build_err);
      }
    }

    if (response.message) {
      this.terminal.echo(this.terminal.colorizeHeader(pre + "Build message:\r\n" + pre) + response.message);
    }

    // new markers
    let newMarkers: Array<Marker> = [];

    this.terminal.echo(this.terminal.colorizeHeader(pre + "Build result:\r\n" + pre))
    if (response.success) {
      this.terminal.success(pre + "Success build.\r\n" + pre);
      this.nzMessageService.success("Compiled successfully.");
      // highlight lines
      newMarkers = newMarkers.concat(this.getMarkersFromLog(response));
    } else {
      this.terminal.error("Build failed.");
      this.nzMessageService.error("Compilation failed.");
      // show code editor
      this.showCodeEditor();
      // highlight or add markers
      newMarkers = newMarkers.concat(this.getMarkersFromLog(response));
      // newMarkers = newMarkers.concat(this.getMarkersFromLog(response, /[0-9]+\:[0-9]+\: ./g, 'error'));
    }
    // remove markers
    this.codeEditor.setMarkers(newMarkers);
    this.terminal.resume();
  }

  getMarkersFromLog(response: BuildProjectResponse): Array<Marker> {
    const error = response.build_err;
    const regex = /[0-9]+\:[0-9]+\:.*\r/g;
    const result = error.match(regex);
    // ["26:5: error: expected identifier or '(' before 'void'
    //   ", "26:5: error: expected ')' before numeric constant
    //   ", "27:6: warning: function declaration isn't a prototype [-Wstrict-prototypes]
    //   ", "42:8: error: lvalue required as left operand of assignment
    //   ", "44:3: warning: statement with no effect [-Wunused-value]
    //   "]
    if (!result) {
      return [];
    }

    const header_offset = response.header_offset;
    const footer_offset = response.footer_offset;
    return result.map(e => {
      // e := <line>:<column>: <severity>: <message>
      const parts = e.split(':').map(v => v.trim());
      // tslint:disable-next-line: radix
      const lineNumber = parseInt(parts[0]) - header_offset; // Removed use of: environment.MAIN_C_FILE_OFFSET;
      let severity: 'error' | 'warning' = 'warning';
      if (parts[2] === 'fatal error') {
        severity = 'error';
      }
      const message = parts[3];
      // console.log(parts);

      return {
        startLineNumber: lineNumber,
        startColumn: 0,
        endLineNumber: lineNumber,
        endColumn: 999,
        message,
        severity
      };
    });
  };

}
