/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProjectSocketSharingService } from './project-socket-sharing.service';

describe('Service: ProjectSocketSharing', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectSocketSharingService]
    });
  });

  it('should ...', inject([ProjectSocketSharingService], (service: ProjectSocketSharingService) => {
    expect(service).toBeTruthy();
  }));
});
