import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject, BehaviorSubject } from 'rxjs';
import { SocketWrapperService, SocketMessage } from 'src/app/core/services/socket/socket-wrapper.service';
import { Project } from 'src/app/core/models/Project.model';
import { NzMessageService } from 'ng-zorro-antd';
import { share, debounceTime } from 'rxjs/operators';

export class CodeChangePayload {
  code_type: 'blockly' | 'code';
  code: string;
}

const CODE_EMITTER_DEBOUNCE_TIME = 500;

@Injectable({
  providedIn: 'root'
})
export class ProjectSocketSharingService implements OnDestroy {
  //
  private socketMessageSub$: Subscription;
  private subjectSub$: Subscription;
  private projectRef: Project;

  private code$ = new BehaviorSubject<CodeChangePayload>(null);
  // emitter for socket emit
  private codeEmitter$ = new BehaviorSubject<CodeChangePayload>(null);
  // sub for emitter
  private codeEmitterSubscription: Subscription;

  constructor(
    private nzMsgService: NzMessageService,
    private socketWrapperService: SocketWrapperService,

  ) {
    this.init();
  }

  ngOnDestroy() {
    if (this.socketMessageSub$) this.socketMessageSub$.unsubscribe();
    if (this.subjectSub$) this.subjectSub$.unsubscribe();
    if (this.codeEmitterSubscription) this.codeEmitterSubscription.unsubscribe();
    if (this.code$) this.code$.complete();
    if (this.codeEmitter$) this.codeEmitter$.complete();
    this.socketWrapperService.leaveRoom();
  }

  init() {
    /*
     * Subscribe to socket messages on init
     */
    this.subjectSub$ = this.socketWrapperService.onNewSubject.subscribe((subj: Subject<SocketMessage>) => {
      if (!subj) {
        // console.log('waiting in project-socket-sharing...');
        return;
      }
      // console.log('reconnect on project-socket-sharing');
      if (this.socketMessageSub$) this.socketMessageSub$.unsubscribe();

      this.socketMessageSub$ = subj.subscribe((m: SocketMessage) => {
        this.processSocketMessage(m);
      });

      // this.joinRoom();
    });

    // subscribe and emit last event in CODE_EMITTER_DEBOUNCE_TIME ms
    this.codeEmitterSubscription = this.codeEmitter$.asObservable().pipe(debounceTime(CODE_EMITTER_DEBOUNCE_TIME)).subscribe(payload => {
      if (payload) {
        // emit!
        this.socketWrapperService.emit("broadcast_code_change", payload);
      }

    })
  }

  /*
   * Change a room on project change.
   */
  public changeRoom(project: Project) {
    this.projectRef = project;
    this.socketWrapperService.leaveRoom();

    if (+this.socketWrapperService.room != this.projectRef.id) {
      this.joinRoom();
    }
  }

  /*
   * Push payload to codeEmitter
   */
  public pushCodeChange(payload: CodeChangePayload) {
    if (this.projectRef.classroom) this.codeEmitter$.next(payload);
  }

  /*
   * Observable for behaviour subject code
   */
  public getCodeObservable() {
    return this.code$.asObservable().pipe(share());
  }

  private joinRoom() {
    if (this.projectRef.classroom_id) {
      this.socketWrapperService.joinRoom(String(this.projectRef.id));
    }
  }

  private processSocketMessage(m: SocketMessage) {
    if (m.type == 'project_code_change') {
      const code = m.payload as CodeChangePayload;
      // emit to BehaviourSubject
      this.code$.next(code);
    } else if (m.type == 'joined_room') {
      const room: string = m.payload["room_name"];
      if (+room == this.projectRef.id) {
        this.nzMsgService.success(`Success socket joined room ${room}`);
      } else {
        this.nzMsgService.error(`Error joining room ${this.projectRef.id}, connected to room ${room} instead :/`);
      }
    } else if (m.type == 'connected') {
      this.joinRoom();
    }
  }
}
