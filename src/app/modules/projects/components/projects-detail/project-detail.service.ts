import { Injectable, OnDestroy, Renderer2, RendererFactory2, OnInit } from '@angular/core';
import { interval, Subscription, Observable, throwError } from 'rxjs';
import { Project } from 'src/app/core/models/Project.model';
import { ProjectsService } from '../../services/projects.service';
import { BuildProjectResponse } from 'src/app/core/models/api-response/build-project-response';
import { tap, catchError, last } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { ProjectSocketSharingService, CodeChangePayload } from './project-socket-sharing.service';
import { DemoProjectsService } from '../../services/demo-projects.service';
import { ProjectApiRequest, TinusaurShield } from 'src/app/core/models/api-request/project-api-request';
import { HttpResponse } from '@angular/common/http';

const autosaveIntervalInSeconds = 10;

declare var Blockly: any;

@Injectable({
  providedIn: 'root'
})
export class ProjectDetailService implements OnDestroy {
  // c source code (if project's type is 'block' - holds c11 code for codeeditor)
  public c11: string;
  // xml blockly DOM representation
  public xml: string;

  // unsaved flag
  public unsaved: boolean = false;

  // processing flags
  public saving: boolean = false;
  public downloading: boolean = false;
  public building: boolean = false;

  // update remote code on next change
  private changedShield = false;

  // emit save in sequence every savingIntervalInSeconds seconds
  private saveInterval = interval(autosaveIntervalInSeconds * 1000);
  private saveIntervalSubscription: Subscription;

  // a element for download purposes
  private downloadLinkElement;

  // current project
  public project: Project;
  public readonly: boolean = true;
  private codeChangeSubscription: Subscription;

  // angular renderer2 instance for downloading purposes
  private renderer: Renderer2;

  constructor(
    private projectSocketSharingService: ProjectSocketSharingService,
    private nzMessageService: NzMessageService,
    private projectsService: ProjectsService,
    private demoProjectsService: DemoProjectsService,
    private router: Router,
    rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
    this.downloadLinkElement = this.renderer.createElement('a');

    // init an autosave subscription
    this.saveIntervalSubscription = this.saveInterval.subscribe(_ => {
      if (this.project.autosave) {
        this.saveCode();
      }
    });
    this.init();
  }

  private bthexContent: string;

  setNewBthexContent(bthexContent: string) {
    this.bthexContent = bthexContent;
  }

  setProject(p: Project) {
    this.project = p;
    if (this.project.type == 'block') {
      this.xml = p.code;
    } else {
      this.c11 = p.code;
    }
    this.unsaved = false;
    this.projectSocketSharingService.changeRoom(this.project);
  }

  ngOnDestroy() {
    // cancel the autosave subscription
    if (this.saveIntervalSubscription) this.saveIntervalSubscription.unsubscribe();
    if (this.codeChangeSubscription) this.codeChangeSubscription.unsubscribe();
    // TODO: remove donwload A element?
  }

  private init() {

    this.codeChangeSubscription = this.projectSocketSharingService.getCodeObservable().subscribe((codeChange: CodeChangePayload) => {
      if (codeChange && this.readonly) {
        if (codeChange.code_type === 'blockly') {
          this.setXml(codeChange.code);
        } else if (codeChange.code_type === 'code') {
          this.setC11(codeChange.code);
        } else {
          console.warn('code_type on CodeChangePayload has a wrong value');
        }
      }
    })
  }

  public setC11(code: string) {
    // update only if changed
    if (this.c11 != code) {
      this.c11 = code;
      if (this.project.type == 'code' && !this.readonly) {
        this.unsaved = true;
        // emit code change
        this.projectSocketSharingService.pushCodeChange({ code: code, code_type: "code" });
      }
    }
  }

  public setXml(xml: string) {
    // update only if changed
    if (this.xml != xml) {
      this.xml = xml;
      if (this.project.type == 'block' && !this.readonly) {
        this.unsaved = true;
        this.projectSocketSharingService.pushCodeChange({ code: xml, code_type: "blockly" });
      }
    }
  }

  private getLastCodeForProject() {
    return this.project.type == 'block' ? this.xml : this.c11;
  }

  public saveCode() {
    if (this.unsaved) {
      this.saving = true;
      const lastCode = this.getLastCodeForProject();
      // unsubscription is handled by ProjectsService
      if (this.project.is_demo) {
        this.project.code = lastCode;
        this.demoProjectsService.saveDemoProject(this.project);
        this.unsaved = false;
        this.saving = false;
      } else {
        this.projectsService.updateCode(this.project, lastCode)
          .subscribe((r: { updated: boolean }) => {
            if (!r.updated) {
              this.nzMessageService.warning('Something went wrong...');
            } else if (this.changedShield) {
              // update shield if changed
              this.changeProject({ shield: this.project.shield });
              this.changedShield = false;
            }

            this.unsaved = false;	// QUESTION: Should this be set to false if the code was not saved?
            this.saving = false;
          },
            error => {
              this.nzMessageService.error("Error saving a project");
              this.unsaved = true;
              this.saving = false;
            });
      }
    }
  }

  public download() {
    if (this.bthexContent) {
      this.downloading = true;
      // TODO
      // el.href = 'data:text/plain;base64,' + bthex;
      var file = new Blob([this.bthexContent], { type: 'text/plain;base64' });
      this.renderer.setAttribute(this.downloadLinkElement, 'href', URL.createObjectURL(file));
      this.renderer.setAttribute(this.downloadLinkElement, 'download', 'main.bthex');
      this.downloadLinkElement.click();
      this.downloading = false;
    } else {
      this.nzMessageService.warning('No BTHEX (binary file) content. Build your project.')
    }
  }

  public build(): Observable<BuildProjectResponse> {
    this.building = true;
    // unsubscription is handled by ProjectsService
    let libs = (Blockly.Tinusaur.libs ? Blockly.Tinusaur.libs : "");	// NOTE: This SHOULD change, should use this.project to keep libraries configuration.
    return this.projectsService.build(this.c11, libs, this.project.type)
      .pipe(
        tap((response: BuildProjectResponse) => {
          this.building = false;
        }),
        catchError(e => {
          this.building = false;
          return throwError(e);
        })
      );
  }

  public changeProjectTitle(newTitle: string) {
    if (newTitle != this.project.title) {
      if (this.project.is_demo) {
        this.nzMessageService.warning('You are not allowed to rename a demo project');
      } else {
        // unsubscription is handled by ProjectsService
        this.projectsService.patch(this.project.id, { title: newTitle })
          .subscribe((p: Project) => this.project = p);
      }
    }
  }

  public changeProjectDescription(newDescription: string) {
    if (newDescription != this.project.description) {
      if (this.project.is_demo) {
        this.nzMessageService.warning('You are not allowed to change a demo project description');
      } else {
        // unsubscription is handled by ProjectsService
        this.projectsService.patch(this.project.id, { description: newDescription })
          .subscribe((p: Project) => this.project = p);
      }
    }
  }

  public changeProjectShield(newShield: TinusaurShield) {
    if (newShield !== this.project.shield) {
      this.changedShield = true;
      this.unsaved = true;
      this.project.shield = newShield;
    }
  }

  public changeProjectStatus(finished: boolean) {
    if (finished !== this.project.finished) {
      this.changeProject({ finished });
    }
  }

  public changeProjectAutosave(autosave: boolean) {
    if (autosave !== this.project.autosave) {
      this.changeProject({ autosave });
    }
  }

  private changeProject(ppo: Partial<Project>) {
    if (this.project.is_demo) {
      this.project = {
        ...this.project,
        ...ppo
      };
      this.demoProjectsService.saveDemoProject(this.project);
    } else {
      // unsubscription is handled by ProjectsService
      this.projectsService.patch(this.project.id, ppo)
        .subscribe((p: Project) => this.project = p, error => {
          this.nzMessageService.error('Error changing project');
          console.error(error);
        });
    }
  }

  public deleteProject() {
    if (this.project) {
      if (this.project.is_demo) {
        this.nzMessageService.warning('You are not allowed to delete a demo project');
      } else {
        // unsubscription is handled by ProjectsService
        this.projectsService.delete(this.project.id)
          .subscribe((_: any) => {
            // this.project = p
            this.router.navigate(['/', 'projects']);
          });
      }
    }
  }

  public migrating = false;

  public migrateCurrentDemoProject() {
    if (this.project.is_demo && !this.migrating) {
      this.migrating = true;
      const projectReq: ProjectApiRequest = {
        title: this.project.title,
        description: this.project.description,
        type: this.project.type,
        shield: this.project.shield,
        images: [],
        tags: this.project.tags.map(v => v.name),
        code: this.project.code
      };
      this.projectsService.create(projectReq)
        .subscribe(event => {
          if (event instanceof HttpResponse) {
            // created
            this.migrating = false;
            const newProject = event.body;
            const id = newProject['id'];
            this.nzMessageService.success(`Successfully migrated demo project to project # ${id}`)
            this.router.navigate(['/', 'projects', 'details', id]);
          }
        }, error => {
          this.nzMessageService.error("Error migrating project");
        })
    }
  }

  public showCopyProjectModal: boolean = false;
  public copying = false;
  public copyCurrentProject(newTitle: string) {
    if (!this.copying) {
      this.copying = true;
      const projectReq: ProjectApiRequest = {
        title: newTitle,
        description: this.project.description,
        type: this.project.type,
        shield: this.project.shield,
        images: [],
        tags: this.project.tags.map(v => v.name),
        code: this.project.code
      };
      this.projectsService.create(projectReq)
        .subscribe(event => {
          if (event instanceof HttpResponse) {
            // created
            this.copying = false;
            this.showCopyProjectModal = false;
            const newProject = event.body;
            const id = newProject['id'];
            this.nzMessageService.success(`Successfully copied project ${this.project.id} to project # ${id}`)
            this.router.navigate(['/', 'projects', 'details', id]);
          }
        }, error => {
          this.nzMessageService.error("Error copying project.");
          this.copying = false;
        })
    }
  }

}
