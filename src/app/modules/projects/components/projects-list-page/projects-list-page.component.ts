import { Component, OnInit } from '@angular/core';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';

@Component({
  selector: 'app-projects-list-page',
  templateUrl: './projects-list-page.component.html',
  styleUrls: ['./projects-list-page.component.scss']
})
export class ProjectsListPageComponent implements OnInit {

  constructor(
    public credentialsService: CredentialsService
  ) { }

  ngOnInit() {
  }

}
