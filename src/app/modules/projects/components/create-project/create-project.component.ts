import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UploadFile, NzMessageService } from 'ng-zorro-antd';
import { ProjectsService } from '../../services/projects.service';
import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ProjectApiRequest, TinusaurShield, TinusaurShieldDescription, DEFAULT_TINUSAUR_SHIELD } from 'src/app/core/models/api-request/project-api-request';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {
  shields = Object.entries(TinusaurShield).map(e => ({ key: e[0], value: TinusaurShieldDescription[e[0]] }));  // shields = Object.keys(TinusaurShield);
  projectForm: FormGroup;
  loading: boolean = false;
  percent: number = 0;

  maxImagesLength: number = 1;

  constructor(
    public credentialsService: CredentialsService,
    private projectsService: ProjectsService,
    private nzMessageService: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.projectForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(900)]],
      type: ['block', [Validators.required, Validators.pattern('block|code')]],
      shield: [DEFAULT_TINUSAUR_SHIELD, [Validators.required, this.shieldValidator]],
      tags: [[], [Validators.required, Validators.minLength(1), Validators.maxLength(4)]],
      //
      classroom: [null],
      // image_1: File, image_2: File, ...
      images: [[], [Validators.minLength(0), Validators.maxLength(this.maxImagesLength)]],
      is_private: [false, [Validators.required]]
    });
  }

  private stateSub: Subscription;
  ngOnDestroy(): void {
    if (this.stateSub) this.stateSub.unsubscribe();
  }

  ngOnInit() {
    // this.stateSub = this.router.events.pipe(
    //   filter(e => e instanceof NavigationStart),
    //   map(() => this.router.getCurrentNavigation().extras.state)
    // )
    this.stateSub = this.route.paramMap
      .pipe(map(() => window.history.state))
      .subscribe(state => {
        if (state && state['classroom']) {
          console.log(state['classroom'])
          this.projectForm.patchValue({
            classroom: state['classroom']
          })
        }
      });
  }


  shieldValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (!(control.value in TinusaurShield)) {
      return { confirm: true, error: true };
    }
    return {};
  };


  submitForm(value: any): void {
    if (this.loading) return;

    for (const key in this.projectForm.controls) {
      this.projectForm.controls[key].markAsDirty();
      this.projectForm.controls[key].updateValueAndValidity();
    }

    if (this.projectForm.valid) {
      // create new project
      this.loading = true;
      let req: ProjectApiRequest = { ...this.projectForm.value };
      if (req.classroom) {
        req.classroom = this.projectForm.controls["classroom"].value.url;
      } else {
        delete req['classroom'];
      }
      this.projectsService.create(req)
        .subscribe(
          (event: HttpEvent<{}>) => {
            if (event.type === HttpEventType.UploadProgress) {
              if (event.total! > 0) {
                this.percent = (event.loaded / event.total!) * 100;
              }
            } else if (event instanceof HttpResponse) {
              // uploaded
              const newProject = event.body;
              const id = newProject['id'];
              this.nzMessageService.success(`Successfully created project ${id}`)
              this.router.navigate(['..', 'details', id], {
                relativeTo: this.route
              });
            }
          },
          err => {
            // fail
            this.nzMessageService.error("Error creating project");
            this.loading = false;
          });
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.projectForm.reset();
    for (const key in this.projectForm.controls) {
      this.projectForm.controls[key].markAsPristine();
      this.projectForm.controls[key].updateValueAndValidity();
    }
  }
}
