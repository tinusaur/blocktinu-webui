import { Injectable } from '@angular/core';
import { Project } from 'src/app/core/models/Project.model';
import { DEFAULT_TINUSAUR_SHIELD } from 'src/app/core/models/api-request/project-api-request';

const blockDemo: Project = {
  id: 'block-demo',
  url: null,
  title: 'Block Project',
  is_demo: true,
  type: 'block',
  owner_id: null,
  owner_name: "You",
  description: "This is your block-based project",
  is_private: true,
  finished: false,
  autosave: false,
  images: [],
  tags: [{ name: "demo", color: '#7ed3b2' }, { name: "block", color: '#caabd8' }],
  code: '',
  libraries: [],
  shield: DEFAULT_TINUSAUR_SHIELD,
  created_at: null,
  updated_at: null,
};

const codeDemo: Project = {
  id: 'code-demo',
  url: null,
  title: 'Code Project',
  is_demo: true,
  type: 'code',
  owner_id: null,
  owner_name: "You",
  description: "This is your C-language project",
  is_private: true,
  finished: false,
  autosave: false,
  images: [],
  tags: [{ name: "demo", color: '#7ed3b2' }, { name: "code", color: '#87a8d0' }],
  code: '',
  libraries: [],
  shield: DEFAULT_TINUSAUR_SHIELD,
  created_at: null,
  updated_at: null,
};

@Injectable({
  providedIn: 'root'
})
export class DemoProjectsService {
  private codeDemoProject: Project;
  private blockDemoProject: Project;

  constructor() {
    const block = this.loadDemoProject('block-demo');
    const code = this.loadDemoProject('code-demo');
    this.blockDemoProject = block || blockDemo;
    this.codeDemoProject = code || codeDemo;
  }

  getCodeDemoProject(): Project {
    return this.codeDemoProject;
  }

  getBlockDemoProject(): Project {
    return this.blockDemoProject;
  }

  getDemoProjects(): Array<Project> {
    return [this.getBlockDemoProject(), this.getCodeDemoProject()];
  }

  saveDemoProject(project: Project) {
    localStorage.setItem('blocktinu-' + project.id, JSON.stringify(project));
  }

  removeDemoProject(project: Project) {
    localStorage.removeItem('blocktinu-' + project.id);
  }

  private loadDemoProject(id: 'block-demo' | 'code-demo'): Project {
    return JSON.parse(localStorage.getItem('blocktinu-' + id)) as Project;
  }
}
