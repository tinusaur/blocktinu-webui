import { Injectable } from '@angular/core';
import { ProjectsService } from './projects.service';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY, of, throwError } from 'rxjs';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Project } from 'src/app/core/models/Project.model';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { CredentialsService } from '../../auth/credentials.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectsResolverService implements Resolve<PaginatedApiResponse<Project>> {

  constructor(
    private projectsService: ProjectsService,
    private credentialsService: CredentialsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<PaginatedApiResponse<Project>> | Observable<never> {

    const query = route.queryParamMap.get('q');
    let ownerId = route.queryParamMap.get('owner_id');
    const page = route.queryParamMap.get('page');
    const page_size = route.queryParamMap.get('page_size');
    const tags = route.queryParamMap.getAll('tags');

    if ('only_current_user_projects' in route.data && route.data.only_current_user_projects && this.credentialsService.isLoggedIn && this.credentialsService.currentUser) {
      const only_current_user_projects = route.data['only_current_user_projects'];
      if (only_current_user_projects) ownerId = String(this.credentialsService.currentUser.id)
    } else if ('owner_id_parent_parameter' in route.data && route.data.owner_id_parent_parameter) {
      // e.g. /users/:userId/projects
      ownerId = route.parent.paramMap.get(route.data.owner_id_parent_parameter);
    }


    const filters = tags.map(tag => {
      return { param: 'tags', value: tag }
    });

    if (query) {
      filters.push({ param: 'search', value: query });
    }

    if (ownerId) {
      filters.push({ param: 'owner_id', value: ownerId });
    }

    if (page) {
      filters.push({ param: 'page', value: page });
    }

    if (page_size) {
      filters.push({ param: 'page_size', value: page_size });
    }

    return this.projectsService.getByFilters(filters).pipe(
      take(1),
      mergeMap((projects: PaginatedApiResponse<Project>) => {
        if (projects) {
          return of(projects);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
