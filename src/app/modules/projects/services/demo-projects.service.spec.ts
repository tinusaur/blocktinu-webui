/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DemoProjectsService } from './demo-projects.service';

describe('Service: DemoProjects', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DemoProjectsService]
    });
  });

  it('should ...', inject([DemoProjectsService], (service: DemoProjectsService) => {
    expect(service).toBeTruthy();
  }));
});
