import { Injectable } from '@angular/core';
import { Project } from 'src/app/core/models/Project.model';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ProjectsService } from './projects.service';
import { Observable, of, EMPTY, throwError } from 'rxjs';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DemoProjectsService } from './demo-projects.service';

@Injectable({
  providedIn: 'root'
})

export class ProjectDetailsResolverService implements Resolve<Project> {


  constructor(private projectsService: ProjectsService, private demoProjectsService: DemoProjectsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Project> | Observable<never> {

    const id = route.paramMap.get('projectId');
    if (id === 'block-demo') {
      return of(this.demoProjectsService.getBlockDemoProject());
    } else if (id === 'code-demo') {
      return of(this.demoProjectsService.getCodeDemoProject());
    }

    return this.projectsService.getById(id).pipe(
      take(1),
      mergeMap((projects: Project) => {
        if (projects) {
          return of(projects);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
