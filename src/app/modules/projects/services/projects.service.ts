import { Injectable } from '@angular/core';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { Observable, throwError } from 'rxjs';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Project } from 'src/app/core/models/Project.model';
import { CustomEncoder } from 'src/app/core/services/custom.encoder';
import { HttpParams, HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, first } from 'rxjs/operators';
import { BuildProjectResponse } from 'src/app/core/models/api-response/build-project-response';
import { ProjectApiRequest } from 'src/app/core/models/api-request/project-api-request';
import { UploadFile } from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private encoder = new CustomEncoder();
  private baseUrl = `${environment.API_URL}/projects`;

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService) {
  }

  public getByFilters(filters: Array<{ param: string, value: string }> = []) {
    let params = new HttpParams({ encoder: new CustomEncoder() });
    for (let item of filters) {
      params = params.append(item.param, item.value);
    }
    const options =
      { params: params };
    return this.http.get<PaginatedApiResponse<Project>>(`${this.baseUrl}/`, options)
      .pipe(
        first(),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return throwError(error);
        })
      );
  }

  public get(page: string, page_size: string, query: string = null): Observable<PaginatedApiResponse<Project>> {
    let params = new HttpParams({ encoder: this.encoder }).set('page', page).set('page_size', page_size);
    if (query) {
      params = params.set('search', query);
    }
    const options =
      { params: params };
    return this.http.get<PaginatedApiResponse<Project>>(`${this.baseUrl}/`, options).pipe(first());
  }

  public getByClassroomId(classroomId: number | string): Observable<PaginatedApiResponse<Project>> {
    const filters = [{ param: 'classroom_id', value: String(classroomId) }];
    return this.getByFilters(filters);
  }

  public getByClassroomIdAndOwnerId(classroomId: number | string, ownerId: number | string): Observable<PaginatedApiResponse<Project>> {
    const filters = [{ param: 'classroom_id', value: String(classroomId) }];
    if (ownerId) filters.push({ param: 'owner_id', value: String(ownerId) });
    return this.getByFilters(filters);
  }

  public getById(id: number | string): Observable<Project> {
    return this.http.get<Project>(`${this.baseUrl}/${id}/`).pipe(first());
  }

  public patch(id: number | string, patchObject: object): Observable<Project> {
    return this.http.patch<Project>(`${this.baseUrl}/${id}/`, patchObject).pipe(first());
  }

  public delete(id: number | string): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${id}/`).pipe(first());
  }

  private generateNewFormData(project: ProjectApiRequest): FormData {
    const formData = new FormData();
    for (var key in project) {
      if (key == 'tags') {
        // tags
        for (let t of project.tags) {
          formData.append('tags', t);
        }
      } else if (key == 'images') {
        // images
        project.images.forEach((value: UploadFile, index: number, array) => {
          if (!value.url && value.originFileObj) {
            formData.append(`image_${index}`, value.originFileObj);
          }
        });
      } else {
        formData.append(key, project[key]);
      }
    }
    return formData;
  }

  // TODO: handle unsubsription
  public create(project: ProjectApiRequest): Observable<HttpEvent<{}>> {
    const formData = this.generateNewFormData(project);
    const url: string = `${this.baseUrl}/`;
    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      withCredentials: true,
    });
    return this.http.request(req).pipe(
      catchError(error => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      })
    );
  }

  public updateCode(project: Project, code: string): Observable<{ updated: boolean }> {
    return this.http.patch<{ updated: boolean }>(`${this.baseUrl}/${project.id}/update_code/`, { code }).pipe(first());
  }

  public build(c11Code: string, libs: string, type: 'block' | 'code'): Observable<BuildProjectResponse> {
    let buildType = type == 'code' ? 'clang' : 'block';
    const data = new FormData();
    // data.set('src', c11Code); // Replaced by 'files[main.c]'
    data.append('files[main.c]', c11Code);
	// Adding other files, if necessary ...
    // data.append('files[file1.c]', 'void f1(void){}'); // NOTE: Placeholder for future implementation.
    // data.append('files[file2.c]', 'void f2(void){}'); // NOTE: Placeholder for future implementation.
    data.set('libs', libs);
    data.set('type', buildType);
    return this.http.post<BuildProjectResponse>(
      `${environment.BUILD_URL}/`,
      data
    ).pipe(first());
  }
}
