import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel.component';
import { NzCarouselModule, NzAlertModule, NzButtonModule, NzAvatarModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,
    NzCarouselModule,
    NzAlertModule,
    NzButtonModule,
    NzAvatarModule
  ],
  declarations: [CarouselComponent],
  exports: [CarouselComponent]
})
export class CarouselModule { }
