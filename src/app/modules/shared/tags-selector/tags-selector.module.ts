import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagsSelectorComponent } from './tags-selector.component';
import { NzInputModule, NzTagModule, NzSelectModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TagsExpandedSelectorComponent } from './tags-expanded-selector/tags-expanded-selector.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzTagModule,
    NzSelectModule,
    NzInputModule
  ],
  declarations: [
    TagsSelectorComponent,
    TagsExpandedSelectorComponent,
  ],
  exports: [
    TagsSelectorComponent,
    TagsExpandedSelectorComponent
  ]
})
export class TagsSelectorModule { }
