import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Tag } from 'src/app/core/models/Tag.models';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { CustomEncoder } from 'src/app/core/services/custom.encoder';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService) {
  }

  public getTagById(id: number): Observable<Tag> {
    return this.http.get<Tag>(`${environment.API_URL}/tags/${id}/`).pipe(first());
  }

  public getTagByName(name: string): Observable<Tag> {
    return this.http.get<Tag>(`${environment.API_URL}/tags/name/${name}/`).pipe(first());
  }

  public getTags(page: string, page_size: string, query: string = null): Observable<PaginatedApiResponse<Tag>> {
    let params = new HttpParams({ encoder: new CustomEncoder() }).set('page', page).set('page_size', page_size);

    if (query) {
      params = params.set('search', query);
    }

    const options =
      { params: params };

    return this.http.get<PaginatedApiResponse<Tag>>(`${environment.API_URL}/tags/`, options).pipe(first());
  }

  public getNextTags(next: string): Observable<PaginatedApiResponse<Tag>> {
    return this.http.get<PaginatedApiResponse<Tag>>(next).pipe(first());
  }

  public createTag(name: string, color: string = null): Observable<Tag> {
    const tag = new Tag();
    tag.name = name;
    if (color) {
      tag.color = color;
    }
    return this.http.post<Tag>(`${environment.API_URL}/tags/`, tag)
      .pipe(
        first(),
        catchError(e => this.errorHandlerService.handleError(e))
      );
  }



}
