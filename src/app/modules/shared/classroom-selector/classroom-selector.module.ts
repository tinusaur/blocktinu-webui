import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomSelectorComponent } from './classroom-selector.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSelectModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzSelectModule,
  ],
  declarations: [ClassroomSelectorComponent],
  exports: [ClassroomSelectorComponent]
})
export class ClassroomSelectorModule { }
