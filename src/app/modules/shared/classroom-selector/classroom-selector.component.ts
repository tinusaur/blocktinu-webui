import { Component, OnInit, forwardRef, OnChanges, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Subscription } from 'rxjs';
import { ClassroomsService } from '../../classrooms/services/classrooms.service';
import { CredentialsService } from '../../auth/credentials.service';

const customValueProvider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ClassroomSelectorComponent),
  multi: true
};


@Component({
  selector: 'app-classroom-selector',
  templateUrl: './classroom-selector.component.html',
  styleUrls: ['./classroom-selector.component.scss'],
  providers: [customValueProvider]
})
export class ClassroomSelectorComponent implements OnInit, ControlValueAccessor {
  //
  // ControlValueAccessor
  propagateChange: any = () => { };

  writeValue(value: Classroom): void {
    if (value) this._selectedClassroom = value;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void { }
  setDisabledState(isDisabled: boolean): void { }
  // /ControlValueAccessor
  //

  loading = false;

  classrooms: PaginatedApiResponse<Classroom>;
  classroomsSub$: Subscription;
  _selectedClassroom: Classroom;

  public set selectedClassroom(v) {
    this._selectedClassroom = v;
    this.onChange.emit(this._selectedClassroom);
    this.propagateChange(this._selectedClassroom);
  }
  public get selectedClassroom() {
    return this._selectedClassroom;
  }


  page = '1';
  page_size = '10';

  @Output() onChange = new EventEmitter<Classroom>();

  constructor(
    private credentialsService: CredentialsService,
    private classroomsService: ClassroomsService) { }

  ngOnInit(): void {
    this.onSearch(null);
 }


  reset() {
    this.selectedClassroom = null;
  }

  public trackById(index, item: Classroom) {
    return item.id;
  }

  onSearch(query: string): void {
    if (!this.credentialsService.currentUser) {
      return;
    }

    this.loading = true;

    const id_in_members = String(this.credentialsService.currentUser.id);
    this.classroomsSub$ = this.classroomsService.get(this.page, this.page_size, query, id_in_members)
      .subscribe(v => {
        this.classrooms = v;
        this.loading = false;
      });

  }

  loadMoreClassrooms(): void {
    if (this.classrooms.next) {
      this.classroomsSub$.unsubscribe();

      this.loading = false;
      this.classroomsSub$ = this.classroomsService.getNext(this.classrooms.next)
        .subscribe(v => {
          this.appendClassrooms(v);
          this.loading = false;
        });
    }
  }

  private appendClassrooms(response: PaginatedApiResponse<Classroom>) {
    if (this.classrooms) {
      const set = new Set([...this.classrooms.results, ...response.results]);
      response.results = Array.from(set.values());
    }
    this.classrooms = response;
  }


}
