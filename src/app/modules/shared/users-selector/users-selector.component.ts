import { Component, OnInit, OnChanges, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { User } from 'src/app/core/models/User.model';
import { Subscription } from 'rxjs';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { UserService } from 'src/app/core/services/user.service';

const customValueProvider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => UsersSelectorComponent),
  multi: true
};


@Component({
  selector: 'app-users-selector',
  templateUrl: './users-selector.component.html',
  styleUrls: ['./users-selector.component.scss'],
  providers: [customValueProvider]
})
export class UsersSelectorComponent implements OnInit, OnChanges, ControlValueAccessor {
  //
  // ControlValueAccessor
  propagateChange: any = () => { };

  writeValue(value: Array<User>): void {
    if (value) this._selectedUsers = value;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void { }
  setDisabledState(isDisabled: boolean): void { }
  // /ControlValueAccessor
  //

  loading = false;

  users: PaginatedApiResponse<User>;
  usersSub$: Subscription;
  _selectedUsers: Array<User> = [];

  public set selectedUsers(v) {
    this._selectedUsers = v;
    this.onChange.emit(this._selectedUsers);
    this.propagateChange(this._selectedUsers);
  }
  public get selectedUsers() {
    return this._selectedUsers;
  }

  page = '1';
  page_size = '10';

  @Input() maxUsersCount: number = 100;
  @Input() defaultUsers: Array<User>;
  @Input() hideUsersWithIds: Array<number> = [];
  @Output() onChange = new EventEmitter<Array<User>>();

  constructor(
    private usersService: UserService) { }

  ngOnInit(): void {
    this.usersSub$ = this.usersService.getUsers('1', '10')
      .subscribe(u => this.users = u);

    if (this.defaultUsers) {
      this.selectedUsers = this.defaultUsers;
    }
  }

  ngOnChanges() {
    if (this.defaultUsers) {
      this.selectedUsers = this.defaultUsers;
    }
  }

  reset() {
    this.selectedUsers = [];
  }

  public trackById(index, item: User) {
    return item.id;
  }


  public isSelected(userId: number) {
    return this.selectedUsers.filter(u => u.id == userId).length > 0;
  }

  onSearch(query: string): void {
    if (!query) return;
    this.loading = true;

    this.usersSub$ = this.usersService.getUsers(this.page, this.page_size, query)
      .subscribe(v => {
        this.users = v;
        this.loading = false;
      });

  }

  loadMoreUsers(): void {
    if (this.users.next) {
      this.usersSub$.unsubscribe();

      this.loading = false;
      this.usersSub$ = this.usersService.getNext(this.users.next)
        .subscribe(v => {
          this.appendUsers(v);
          this.loading = false;
        });
    }
  }

  private appendUsers(response: PaginatedApiResponse<User>) {
    if (this.users) {
      const set = new Set([...this.users.results, ...response.results]);
      response.results = Array.from(set.values());
    }
    this.users = response;
  }


}
