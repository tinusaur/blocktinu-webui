import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersSelectorComponent } from './users-selector.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSelectModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzSelectModule,
  ],
  declarations: [UsersSelectorComponent],
  exports: [UsersSelectorComponent]
})
export class UsersSelectorModule { }
