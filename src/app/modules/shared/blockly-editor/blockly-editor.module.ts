import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlocklyEditorComponent } from './blockly-editor.component';
import { NgxBlocklyModule } from 'ngx-blockly';
import { TinusaurNgxBlocklyComponent } from './components/tinusaur-ngx-blockly/tinusaur-ngx-blockly.component';


@NgModule({
  imports: [
    CommonModule,
    NgxBlocklyModule
  ],
  declarations: [
    BlocklyEditorComponent,
    TinusaurNgxBlocklyComponent
  ],
  exports: [
    BlocklyEditorComponent
  ]
})
export class BlocklyEditorModule { }
