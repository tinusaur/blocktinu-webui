import { Component, OnInit, ViewChild, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgxBlocklyGeneratorConfig } from 'ngx-blockly';
import { TinusaurNgxBlocklyComponent, TinusaurNgxBlocklyConfig } from './components/tinusaur-ngx-blockly/tinusaur-ngx-blockly.component';
import { getToolboxConfigForShield } from './toolbox/toolbox';
import { TinusaurShield } from 'src/app/core/models/api-request/project-api-request';

@Component({
  selector: 'app-blockly-editor',
  templateUrl: './blockly-editor.component.html',
  styleUrls: ['./blockly-editor.component.scss']
})
export class BlocklyEditorComponent implements OnInit, OnChanges {

  @ViewChild(TinusaurNgxBlocklyComponent, { static: false }) tinusaurBlocklyEditor: TinusaurNgxBlocklyComponent;

  @Output() onC11: EventEmitter<string> = new EventEmitter<string>();
  @Output() onXml: EventEmitter<string> = new EventEmitter<string>();

  @Input() shield: TinusaurShield;
  @Input() readonly: boolean = true;
  // XML
  @Input() public inputXmlCode: string;

  public config: TinusaurNgxBlocklyConfig = {
    toolbox: getToolboxConfigForShield(this.shield),
    shield: this.shield,
    scrollbars: true,
    trashcan: true,
    zoom:
    {
      controls: true,
      wheel: true,
      startScale: 1.0,
      maxScale: 3,
      minScale: 0.3,
      scaleSpeed: 1.2
    },
    readOnly: this.readonly
  };

  public generatorConfig: NgxBlocklyGeneratorConfig = {};

  public onTinusaurCode(code: string) {
    this.onC11.emit(code);
  }

  public onTinusaurXml(xml: string) {
    this.onXml.emit(xml);
  }

  public resize() {
    if (this.tinusaurBlocklyEditor) {
      this.tinusaurBlocklyEditor.onResize(null);
    }
  }

  constructor() {}

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('shield' in changes && changes['shield'].previousValue != changes['shield'].currentValue) {
      this.changeToolboxForPlatform(this.shield);
    }
    if ('readonly' in changes && changes['readonly'].previousValue != changes['readonly'].currentValue) {
      this.changeReadOnlyFlag(this.readonly);
    }
  }

  private changeToolboxForPlatform(shield: TinusaurShield) {
    this.config = { ...this.config, toolbox: getToolboxConfigForShield(shield), shield: shield };
  }

  private changeReadOnlyFlag(readonly: boolean) {
    this.config = { ...this.config, readOnly: readonly };
  }

}
