import { variablesCategory } from './categories/variables';
import { functionCategory } from './categories/function';
import { controlsCategory } from './categories/controls';
import { logicCategory } from './categories/logic';
import { mathCategory } from './categories/math';
import { separator } from './categories/separator';
import { tinusaurCategory } from './categories/tinusaur';
import { tinusaurLedx2ShieldsCategory } from './categories/tinusaur-ledx2-shields.category';
import { libraryCategory } from './categories/library';
import { toolsCategory } from './categories/tools';
import { experimentalCategory } from './categories/experimental';

import { ToolboxConfig } from './toolbox-config';

export class Ledx2ToolboxConfig extends ToolboxConfig {
  structure = [
    variablesCategory,
    functionCategory,
    controlsCategory,
    logicCategory,
    mathCategory,
    separator,
    tinusaurCategory,
    tinusaurLedx2ShieldsCategory,
    libraryCategory,
    toolsCategory,
    experimentalCategory
  ];
};
