export class ToolboxConfig {
  structure: Array<string>;

  getConfig(): string {
    return `
    <xml id="toolbox" style="display: none">
    ${this.buildStructure()}
    </xml>
  `;
  }

  private buildStructure() {
    // let result: string = '';

    // for (const el in this.structure) {
    //   result += el;
    // }

    return this.structure.join('\n');

    // return result;
  }

}
