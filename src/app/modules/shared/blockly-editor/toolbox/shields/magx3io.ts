import { variablesCategory } from './categories/variables';
import { functionCategory } from './categories/function';
import { controlsCategory } from './categories/controls';
import { logicCategory } from './categories/logic';
import { mathCategory } from './categories/math';
import { separator } from './categories/separator';
import { tinusaurCategory } from './categories/tinusaur';
import { tinusaurMagx3ioShieldsCategory } from './categories/tinusaur-magx3io-shields.category';
import { libraryCategory } from './categories/library';
import { toolsCategory } from './categories/tools';
import { experimentalCategory } from './categories/experimental';

import { ToolboxConfig } from './toolbox-config';

export class Magx3ioToolboxConfig extends ToolboxConfig {
  structure = [
    variablesCategory,
    functionCategory,
    controlsCategory,
    logicCategory,
    mathCategory,
    separator,
    tinusaurCategory,
    tinusaurMagx3ioShieldsCategory,
    libraryCategory,
    toolsCategory,
    experimentalCategory
  ];
};
