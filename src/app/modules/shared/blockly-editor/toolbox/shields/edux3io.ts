import { variablesCategory } from './categories/variables';
import { functionCategory } from './categories/function';
import { controlsCategory } from './categories/controls';
import { logicCategory } from './categories/logic';
import { mathCategory } from './categories/math';
import { separator } from './categories/separator';
import { tinusaurCategory } from './categories/tinusaur';
import { tinusaurEdux3ioShieldsCategory } from './categories/tinusaur-edux3io-shields.category';
import { libraryCategory } from './categories/library';
import { toolsCategory } from './categories/tools';
import { experimentalCategory } from './categories/experimental';

import { ToolboxConfig } from './toolbox-config';

export class Edux3ioToolboxConfig extends ToolboxConfig {
  structure = [
    variablesCategory,
    functionCategory,
    controlsCategory,
    logicCategory,
    mathCategory,
    separator,
    tinusaurCategory,
    tinusaurEdux3ioShieldsCategory,
    libraryCategory,
    toolsCategory,
    experimentalCategory
  ];
};
