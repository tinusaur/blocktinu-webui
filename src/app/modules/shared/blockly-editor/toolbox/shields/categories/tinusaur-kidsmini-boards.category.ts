export const tinusaurKidsminiBoardsCategory = `
<category name="Board KidsMini" colour="10">
    <block type="tinusaur_board_kidsmini_led_white"></block>
    <block type="tinusaur_board_kidsmini_button"></block>
    <block type="tinusaur_board_kidsmini_buzzer">
		<value name="PERIOD"><block type="math_number"><field name="NUM">200</field></block></value>
		<value name="REPEATS"><block type="math_number"><field name="NUM">200</field></block></value>
    </block>
    <block type="tinusaur_board_kidsmini_servo">
		<value name="SERVO_POS"><block type="math_number"><field name="NUM">125</field></block></value>
		<value name="SERVO_REPEAT"><block type="math_number"><field name="NUM">10</field></block></value>
	</block>
    <block type="tinusaur_board_kidsmini_photores"></block>
</category>
`;
