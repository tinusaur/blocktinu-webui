export const tinusaurCategory = `
<category name="Tinusaur" colour="120">
    <block type="tinusaur_board_main_button"></block>
    <block type="tinusaur_shield_ledx1_pb0"></block>
    <block type="tinusaur_shield_ledx1"></block>
<!--
    <block type="tinusaur_board_main_comout"></block>
    <block type="tinusaur_board_main_cominp"></block>
    <block type="tinusaur_buildin_led"></block>
    <block type="tinusaur_analog_write"></block>
    <block type="tinusaur_analog_read"></block>
    <block type="tinusaur_tone"></block>
    <block type="tinusaur_notone"></block>
    <block type="tinusaur_highlow"></block>
    <block type="tinusaur_serial_print">
        <value name="CONTENT">
            <block type="text">
                <field name="TEXT">Hello Tinusaur</field>
            </block>
        </value>
    </block>
 -->
</category>
`;
