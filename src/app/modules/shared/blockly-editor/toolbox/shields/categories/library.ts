export const libraryCategory = `
<category name="Library" colour="60">
    <block type="tinusaur_delay_ms"></block>
    <block type="tinusaur_delay_us"></block>
    <block type="library_tinyavrlib_scheduler"></block>
    <block type="library_tinyavrlib_adcx_read"></block>
    <block type="library_tinyavrlib_adcx_temp"></block>
    <block type="tinusaur_output_portb"></block>
    <block type="tinusaur_input_portb"></block>
    <block type="advanced_nop"></block>
</category>
`;
