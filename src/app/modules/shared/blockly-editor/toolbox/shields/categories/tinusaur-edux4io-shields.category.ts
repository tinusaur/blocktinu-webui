export const tinusaurEdux4ioShieldsCategory = `
<category name="Shield EDUx4IO" colour="10">
    <block type="tinusaur_shield_edux4io_led"></block>
    <block type="tinusaur_shield_edux4io_buzzer">
		<value name="PERIOD"><block type="math_number"><field name="NUM">200</field></block></value>
		<value name="REPEATS"><block type="math_number"><field name="NUM">200</field></block></value>
    </block>
    <block type="tinusaur_shield_edux4io_button"></block>
    <block type="tinusaur_shield_edux4io_photores"></block>
</category>
`;
