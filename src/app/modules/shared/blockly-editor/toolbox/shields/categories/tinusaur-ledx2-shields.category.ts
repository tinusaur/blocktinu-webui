export const tinusaurLedx2ShieldsCategory = `
<category name="Shield LEDx2" colour="10">
    <block type="tinusaur_shield_ledx2_led_red"></block>
    <block type="tinusaur_shield_ledx2_led_green"></block>
    <block type="tinusaur_shield_ledx2_servo">
		<value name="SERVO_POS"><block type="math_number"><field name="NUM">125</field></block></value>
		<value name="SERVO_REPEAT"><block type="math_number"><field name="NUM">10</field></block></value>
	</block>
    <block type="tinusaur_common_coms_sig"></block>
    <block type="tinusaur_common_coms_chk"></block>
    <block type="tinusaur_common_coms_lsn"></block>
    <block type="tinusaur_shield_ledx2_extout"></block>
    <block type="tinusaur_shield_ledx2_extinp"></block>
</category>
`;
