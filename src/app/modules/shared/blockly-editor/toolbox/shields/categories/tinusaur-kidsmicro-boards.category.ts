export const tinusaurKidsmicroBoardsCategory = `
<category name="Board KidsMicro" colour="10">
    <block type="tinusaur_board_kidsmicro_led_red"></block>
    <block type="tinusaur_board_kidsmicro_led_green"></block>
    <block type="tinusaur_board_kidsmicro_button"></block>
    <block type="tinusaur_board_kidsmicro_buzzer">
		<value name="PERIOD"><block type="math_number"><field name="NUM">200</field></block></value>
		<value name="REPEATS"><block type="math_number"><field name="NUM">200</field></block></value>
    </block>
    <block type="tinusaur_board_kidsmicro_photores"></block>
</category>
`;
