export const tinusaurExamplesCategory = `
<category name="Examples" colour="150">

    <category name="Blinking, 1 LED (Shield LEDx2)" colour="160">
        <block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED1</field><field name="STAT">LOW</field><next><block type="tinusaur_delay_ms"><value name="DELAY_TIME"><block type="math_number"><field name="NUM">500</field></block></value><next><block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED1</field><field name="STAT">HIGH</field><next><block type="tinusaur_delay_ms"><value name="DELAY_TIME"><block type="math_number"><field name="NUM">500</field></block></value></block></next></block></next></block></next></block>
    </category>

    <category name="Blinking, 2 LEDs (Shield LEDx2)" colour="400">
        <block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED1</field><field name="STAT">LOW</field><next><block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED2</field><field name="STAT">HIGH</field><next><block type="tinusaur_delay_ms"><value name="DELAY_TIME"><block type="math_number"><field name="NUM">500</field></block></value><next><block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED1</field><field name="STAT">HIGH</field><next><block type="tinusaur_shield_ledx2"><field name="PIN">SHIELD_LEDX2_LED2</field><field name="STAT">LOW</field><next><block type="tinusaur_delay_ms"><value name="DELAY_TIME"><block type="math_number"><field name="NUM">500</field></block></value></block></next></block></next></block></next></block></next></block></next></block>
    </category>

</category>
`;


