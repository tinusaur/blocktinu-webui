export const tinusaurEdux3ioShieldsCategory = `
<category name="Shield EDUx3IO" colour="10">
    <block type="tinusaur_shield_edux3io_led"></block>
    <block type="tinusaur_shield_edux3io_buzzer">
		<value name="PERIOD"><block type="math_number"><field name="NUM">200</field></block></value>
		<value name="REPEATS"><block type="math_number"><field name="NUM">200</field></block></value>
	</block>
    <block type="tinusaur_shield_edux3io_photores"></block>
    <block type="tinusaur_shield_edux3io_servo">
		<value name="SERVO_POS"><block type="math_number"><field name="NUM">125</field></block></value>
		<value name="SERVO_REPEAT"><block type="math_number"><field name="NUM">10</field></block></value>
	</block>
    <block type="tinusaur_common_coms_sig"></block>
    <block type="tinusaur_common_coms_chk"></block>
    <block type="tinusaur_common_coms_lsn"></block>
</category>
`;
