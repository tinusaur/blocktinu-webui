export const tinusaurLedx3ShieldsCategory = `
<category name="Shield LEDx3" colour="10">
    <block type="tinusaur_shield_ledx3_led_red"></block>
    <block type="tinusaur_shield_ledx3_led_yellow"></block>
    <block type="tinusaur_shield_ledx3_led_green"></block>
    <block type="tinusaur_shield_ledx3_servo">
		<value name="SERVO_POS"><block type="math_number"><field name="NUM">125</field></block></value>
		<value name="SERVO_REPEAT"><block type="math_number"><field name="NUM">10</field></block></value>
	</block>
    <block type="tinusaur_common_coms_sig"></block>
    <block type="tinusaur_common_coms_chk"></block>
    <block type="tinusaur_common_coms_lsn"></block>
</category>
`;
