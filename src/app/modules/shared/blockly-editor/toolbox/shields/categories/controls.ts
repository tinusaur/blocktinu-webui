export const controlsCategory = `
<category name="Controls" colour="20">
    <block type="controls_repeat_forever"></block>
    <block type="controls_if"></block>
    <block type="controls_while_do"></block>
    <block type="controls_do_while"></block>
    <block type="controls_for_do"></block>
    <block type="controls_break"></block>
    <block type="controls_continue"></block>
    <block type="controls_exit"></block>
	<!-- <sep gap="48"></sep> -->
    <!-- <block type="controls_for"></block> -->
    <!-- <block type="controls_whileUntil"></block> -->
</category>
`;
