export const experimentalCategory = `
<category name="Experimental" colour="190">
    <block type="tinusaur_shield_motox4_motor1"></block>
    <block type="tinusaur_shield_motox4_motor2"></block>
    <block type="tinusaur_shield_motox4_servomots">
		<value name="SERVOMOT1_VAL"><block type="math_number"><field name="NUM">0</field></block></value>
		<value name="SERVOMOT2_VAL"><block type="math_number"><field name="NUM">0</field></block></value>
	</block>
    <block type="tinusaur_shield_motox4_irleds"></block>
    <block type="tinusaur_shield_motox4_irsens"></block>
    <block type="tinusaur_shield_ledx12">
		<value name="CHRLPLXNG_LED"><block type="math_number"><field name="NUM">1</field></block></value>
	</block>
    <block type="tinusaur_shield_ledx12_off"></block>
    <block type="tinusaur_shield_oledx1_clear"></block>
    <block type="tinusaur_shield_oledx1_fill"></block>
    <block type="tinusaur_shield_oledx1_text"></block>
    <block type="tinusaur_shield_oledx1_nump">
		<value name="NUM"><block type="math_number"><field name="NUM">0</field></block></value>
	</block>
    <block type="sourcecode_testing"></block>
</category>
`;
