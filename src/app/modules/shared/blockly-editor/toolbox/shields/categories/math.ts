export const mathCategory = `
<category name="Math" colour="230">
    <block type="math_number"></block>
    <block type="math_arithmetic"></block>
    <block type="math_number_property"></block>
</category>
`;
