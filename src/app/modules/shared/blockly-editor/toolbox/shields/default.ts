import { variablesCategory } from './categories/variables';
import { functionCategory } from './categories/function';
import { controlsCategory } from './categories/controls';
import { logicCategory } from './categories/logic';
import { mathCategory } from './categories/math';
import { separator } from './categories/separator';
import { tinusaurCategory } from './categories/tinusaur';
import { tinusaurExamplesCategory } from './categories/tinusaur-examples';
import { libraryCategory } from './categories/library';
import { toolsCategory } from './categories/tools';
import { experimentalCategory } from './categories/experimental';

import { ToolboxConfig } from './toolbox-config';

export class DefaultToolboxConfig extends ToolboxConfig {
  structure = [
    variablesCategory,
    functionCategory,
    controlsCategory,
    logicCategory,
    mathCategory,
    separator,
    tinusaurCategory,
    tinusaurExamplesCategory,
    libraryCategory,
    toolsCategory,
    experimentalCategory
  ];
};
