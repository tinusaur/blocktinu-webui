import { TinusaurShield } from 'src/app/core/models/api-request/project-api-request';

import { Ledx2ToolboxConfig } from './shields/ledx2';
import { Ledx3ToolboxConfig } from './shields/ledx3';
import { Edux3ioToolboxConfig } from './shields/edux3io';
import { Magx3ioToolboxConfig } from './shields/magx3io';
import { KidsmicroToolboxConfig } from './shields/kidsmicro';
import { KidsminiToolboxConfig } from './shields/kidsmini';
import { DefaultToolboxConfig } from './shields/default';

export const getToolboxConfigForShield = (shield: TinusaurShield) => {
  switch (shield) {
    case TinusaurShield.ledx2:
      return new Ledx2ToolboxConfig().getConfig();
    case TinusaurShield.ledx3:
      return new Ledx3ToolboxConfig().getConfig();
    case TinusaurShield.edux3io:
      return new Edux3ioToolboxConfig().getConfig();
    case TinusaurShield.magx3io:
      return new Magx3ioToolboxConfig().getConfig();
    case TinusaurShield.kidsmicro:
      return new KidsmicroToolboxConfig().getConfig();
    case TinusaurShield.kidsmini:
      return new KidsminiToolboxConfig().getConfig();
    default:
      return new DefaultToolboxConfig().getConfig();
  }
};
