## Blocktinu toolbox

#### How to change a toolbox for an existing shield:
Open `shields/<shield identifier>.ts`:
```typescript
// imports
// ...
export class Ledx2ToolboxConfig extends ToolboxConfig {
  structure = [
    variablesCategory,
    functionCategory,
    // ...
    tinusaurLedx2ShieldsCategory,
    // ...
  ];
};
```
`structure` variable defines array of strings, which will be converted into blockly toolbox config.
Each element of `structure` is xml string
(see https://developers.google.com/blockly/guides/configure/web/toolbox).
```typescript
export const mathCategory = `
<category name="Math" colour="230">
    <block type="math_number"></block>
    <block type="math_arithmetic"></block>
</category>
`
```
So far you can define or edit categories in `shields/categories` or re-define the toolbox structure for a given shield by changing `structure` variable.
*Note*: Categories in `shields/categories` are reusable and changing them will affect all toolboxes they're used in

#### How to define a new toolbox and a new shield:

1. Create a new file `<shield identifier>.ts` in `sheilds` folder

```typescript
import { variablesCategory } from './categories/variables';
import { functionCategory } from './categories/function';
import { controlsCategory } from './categories/controls';
// ...

export class shieldIdentifierToolboxConfig extends ToolboxConfig {
  // define toolbox structure
  structure = [
    variablesCategory,
    functionCategory,
    controlsCategory,
    // ...
  ];
};
```
2. Add the shield declaration in `src/app/core/models/api-request/project-api-request:TinusaurShield`:
```typescript
// ...
export enum TinusaurShield {
  ledx2 = 'ledx2',
  //...
  <shield identifier> = <shield identifier>''
}
// ...
``` 

3. Add the shield definition in `./toolbox.ts`:
```typescript
// import ShieldIdentifierToolboxConfig
import { ShieldIdentifierToolboxConfig } from './shields/<shield identifier>';

// add a new case to switch statement
// ...
    case TinusaurShield.shieldIdentifier:
      return new ShieldIdentifierToolboxConfig().getConfig();
// ...
```
