import { Component, Output, EventEmitter, ViewChild, ElementRef, Renderer2, OnChanges, SimpleChanges, Input } from '@angular/core';
import { NgxBlocklyComponent } from 'ngx-blockly';
import { ReplaySubject } from 'rxjs';
import { first } from 'rxjs/operators';

export declare class TinusaurNgxBlocklyConfig {
  collapse?: boolean;
  comments?: boolean;
  css?: boolean;
  disable?: boolean;
  grid?: {
    spacing: number;
    length: number;
    colour: string;
    snap: boolean;
  };
  horizontalLayout?: boolean;
  maxBlocks?: number;
  maxInstances?: object;
  media?: string;
  oneBasedIndex?: boolean;
  readOnly?: boolean;
  rtl?: boolean;
  scrollbars?: boolean;
  sounds?: boolean;
  theme?: any;
  toolbox?: string;
  toolboxPosition?: string;
  trashcan?: boolean;
  maxTrashcanContents?: number;
  zoom?: {
    controls: boolean;
    wheel: boolean;
    startScale: number;
    maxScale: number;
    minScale: number;
    scaleSpeed: number;
  };

  shield?: string;
}

declare var Blockly: any;

@Component({
  selector: 'tinusaur-ngx-blockly',
  templateUrl: './tinusaur-ngx-blockly.component.html',
  styleUrls: ['./tinusaur-ngx-blockly.component.scss']
})
export class TinusaurNgxBlocklyComponent extends NgxBlocklyComponent implements OnChanges {
  private iconsPath = 'assets/images/tinusaur/toolbox/';
  @Input() config: TinusaurNgxBlocklyConfig;

  constructor(
    private renderer: Renderer2
  ) {
    super();
  }
  private workspace$: ReplaySubject<any> = new ReplaySubject<any>();
  @Output() public tinusaurCode: EventEmitter<string> = new EventEmitter<string>();
  @Output() public xmlCode: EventEmitter<string> = new EventEmitter<string>();
  // XML
  @Input() public inputXmlCode: string;

  @ViewChild('blockly', { static: false }) blocklyEl: ElementRef;
  @ViewChild('toolboxTrigger', { static: false }) toolboxTriggerEl: ElementRef;

  ngAfterViewInit() {
    Blockly.Scrollbar.scrollbarThickness = 10;
    super.ngAfterViewInit();

    if (!this.config.readOnly) {
      // Blockly toolbox icons
      this.workspace.toolbox_.tree_.children_.forEach(treeRow => this.processBlocklyTreeRow(treeRow));
    }

    this.updateConfig();
    this.workspace.registerButtonCallback('clearAllBlocks', () => this.workspace.clear());
    this.workspace$.next(this.workspace);
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('config' in changes && changes['config'].previousValue != changes['config'].currentValue) {
      this.updateConfig();
    }

    if ('inputXmlCode' in changes && changes['inputXmlCode'].previousValue != changes['inputXmlCode'].currentValue) {
      this.xmlToWorkspace(this.inputXmlCode, changes['inputXmlCode'].firstChange);
    }
  }

  private updateConfig() {
    if (this.workspace) {
      this.updateToolbox();
      // See https://github.com/google/blockly/issues/3049
      // this.workspace.setReadOnly(this.config.readOnly);
    }
  }

  // Format: "shield:ledx2", "shield:ledx2,ledx3", "shield:*" or similar.
  // Returns list of shields the specified block is associated with (or "*" for all).
  private getBlockShieldsAssoc(block): Array<string> {
    if (block.assoc) {
      const split = block.assoc.split(':');
      if (split.length > 1) {
        return split[1].split(',');
      }
    }
    return [];
  }

  private updateToolbox() {
    // workspace and not readonly ( toolbox exists )
    if (this.workspace && this.workspace.toolbox_) {
      this.workspace.updateToolbox(this.config.toolbox);
      this.workspace.toolbox_.tree_.children_.forEach(treeRow => this.processBlocklyTreeRow(treeRow));

      let shields: Array<string> = [];
      this.workspace.getAllBlocks().forEach(block => {
        shields = this.getBlockShieldsAssoc(block);
        if (shields.length > 0) {
          block.setEnabled(
		    shields.includes(this.config.shield)
			|| shields.includes('*'));
        }
      })
    }
  }

  shadeColor2(color, percent) {
    var f = parseInt(color.slice(1), 16), t = percent < 0 ? 0 : 255, p = percent < 0 ? percent * -1 : percent, R = f >> 16, G = f >> 8 & 0x00FF, B = f & 0x0000FF;
    return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
  }

  processBlocklyTreeRow(treeRow) {
    treeRow.element_.onmouseover = () => {
      treeRow.element_.style.opacity = 0.8;
    };

    treeRow.element_.onclick = () => {
      // shade a color and change svg background fill
      // this.workspace.toolbox.flyout_.svgBackground_.style.fill = this.shadeColor2(treeRow.hexColour, .8);
      // document.getElementsByClassName('blocklySvg')[0].style.filter = 'blur(' + 5 + 'px)';
    }

    treeRow.element_.onmouseout = () => {
      treeRow.element_.style.opacity = 1;
    };
    treeRow.element_.style.backgroundColor = treeRow.hexColour;

    const filename = treeRow.getIconElement().nextSibling.innerText.replace(" ", "_").toLowerCase();
    if (filename.length > 0) {
      treeRow.getIconElement().style.backgroundImage = 'url(' + this.iconsPath + filename + '.png)';
    }
  }

  public workspaceToCode(workspaceId: string) {
    this.tinusaurCode.emit(Blockly.Tinusaur.workspaceToCode(Blockly.Workspace.getById(workspaceId)));
    this.xmlCode.emit(Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.Workspace.getById(workspaceId))));
  }

  private xmlToWorkspace(xml, recenter = false) {
    // Blockly.Tinusaur.codeToWorkspace(Blockly.Workspace.getById(workspaceId)));
    // this.xmlCode.emit(Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.Workspace.getById(workspaceId))));
    if (this.workspace) {
      this.xmlToWorkspaceHelper(xml, recenter);
    } else {
      // unsubscribes right after first result
      this.workspace$.pipe(first()).subscribe(w => {
        this.xmlToWorkspaceHelper(xml, recenter);
      });
    }
  }

  private xmlToWorkspaceHelper(xml, recenter) {
    this.workspace.clear();
    const dom = Blockly.Xml.textToDom(xml);
    Blockly.Xml.domToWorkspace(dom, this.workspace);
    if (recenter) {
      this.workspace.setScale(this.workspace.options.zoomOptions.startScale);
      this.workspace.scrollCenter();
    }
  }

  public toolboxFolded: boolean = false;
  public onToolboxTrigger(event) {
    const currentElement = this.toolboxTriggerEl.nativeElement;
    const nextEl = this.renderer.nextSibling(currentElement);
    this.toolboxFolded = !this.toolboxFolded;
    if (this.toolboxFolded) {
      this.renderer.removeClass(nextEl, 'folded');
      this.renderer.removeClass(currentElement, 'folded');
    } else {
      this.renderer.addClass(nextEl, 'folded');
      this.renderer.addClass(currentElement, 'folded');
    }
  }
}
