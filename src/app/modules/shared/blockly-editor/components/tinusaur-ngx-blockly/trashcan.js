/**
 * Blocktinu
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * Copyright (c) 2018 The Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/blocktinu
 *
 */


'use strict';

// export default function initBlocklyTrashcanClass() {

/**
* @license
* Visual Blocks Editor
*
* Copyright 2011 Google Inc.
* https://developers.google.com/blockly/
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
 * @fileoverview Object representing a trash can icon.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.Trashcan');

goog.require('Blockly.utils');

goog.require('goog.math.Rect');

/**
 * Class for a trash can.
 * @param {!Blockly.Workspace} workspace The workspace to sit in.
 * @constructor
 */
Blockly.Trashcan = function (workspace) {
  this.workspace_ = workspace;
};

/**
 * Width of both the trash can and lid images.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.WIDTH_ = 999999;//'100%';

/**
 * Height of the trashcan image (minus lid).
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.BODY_HEIGHT_ = 50;

/**
 * Height of the lid image.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.LID_HEIGHT_ = 16;

/**
 * Distance between trashcan and bottom edge of workspace.
 * @type {number}
 * @private
 */
// CHANGED
Blockly.Trashcan.prototype.MARGIN_BOTTOM_ = 0; // 20

/**
 * Distance between trashcan and right edge of workspace.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.MARGIN_SIDE_ = 0; // 20

/**
 * Extent of hotspot on all sides beyond the size of the image.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.MARGIN_HOTSPOT_ = 10;

/**
 * Location of trashcan in sprite image.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.SPRITE_LEFT_ = 0;

/**
 * Location of trashcan in sprite image.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.SPRITE_TOP_ = 32;

/**
 * Current open/close state of the lid.
 * @type {boolean}
 */
Blockly.Trashcan.prototype.isOpen = false;

/**
 * Current hidden state of the lid.
 * @type {boolean}
 */
Blockly.Trashcan.prototype.isHidden = true;


/**
 * The SVG group containing the trash can.
 * @type {Element}
 * @private
 */
Blockly.Trashcan.prototype.svgGroup_ = null;

/**
 * The SVG image element of the trash can lid.
 * @type {Element}
 * @private
 */
Blockly.Trashcan.prototype.svgLid_ = null;

/**
 * Task ID of opening/closing animation.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.lidTask_ = 0;

/**
 * Current state of lid opening (0.0 = closed, 1.0 = open).
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.lidOpen_ = 0;

/**
 * Left coordinate of the trash can.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.left_ = 0;

/**
 * Top coordinate of the trash can.
 * @type {number}
 * @private
 */
Blockly.Trashcan.prototype.top_ = 0;

/**
 * Create the trash can elements.
 * @return {!Element} The trash can's SVG group.
 */
Blockly.Trashcan.prototype.createDom = function () {
  this.svgGroup_ = Blockly.utils.dom.createSvgElement('g',
    {
      'class': 'blocklyTrash',
      'id': 'blocklyTrashSvg',
      'opacity': '0',
      // 'display': 'none'
    }, null);
  var rect = Blockly.utils.dom.createSvgElement('rect',
    {
      'width': '100%',//this.WIDTH_,
      'height': this.BODY_HEIGHT_,
      'y': this.LID_HEIGHT_,
      'x': '0',
      'fill': 'red'
    }, this.svgGroup_);
  //   <text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle">TEXT</text>

  var txt = Blockly.utils.dom.createSvgElement('text', {
    'x': '55%',
    'y': this.LID_HEIGHT_ + this.BODY_HEIGHT_ / 2,
    //   'width': '100%',
    //   'height': '100%',
    'fill': 'white',
    'alignment-baseline': 'middle',
    'text-anchor': 'middle'
  }, this.svgGroup_);
  txt.innerHTML = 'Remove';
  Blockly.bindEventWithChecks_(this.svgGroup_, 'mouseup', this, this.click);
  this.animateLid_();
  return this.svgGroup_;
};

/**
 * Initialize the trash can.
 * @param {number} bottom Distance from workspace bottom to bottom of trashcan.
 * @return {number} Distance from workspace bottom to the top of trashcan.
 */
Blockly.Trashcan.prototype.init = function (bottom) {
  this.bottom_ = this.MARGIN_BOTTOM_ + bottom;
  this.setOpen_(false);
  return this.bottom_ + this.BODY_HEIGHT_ + this.LID_HEIGHT_;
};

/**
 * Dispose of this trash can.
 * Unlink from all DOM elements to prevent memory leaks.
 */
Blockly.Trashcan.prototype.dispose = function () {
  if (this.svgGroup_) {
    Blockly.utils.removeNode(this.svgGroup_);
    this.svgGroup_ = null;
  }
  this.svgLid_ = null;
  this.workspace_ = null;
  clearTimeout(this.lidTask_);
};

/**
 * Move the trash can to the bottom-right corner.
 */
Blockly.Trashcan.prototype.position = function () {
  var metrics = this.workspace_.getMetrics();
  if (!metrics) {
    // There are no metrics available (workspace is probably not visible).
    return;
  }
  //   if (this.workspace_.RTL) {
  this.left_ = this.MARGIN_SIDE_ + Blockly.Scrollbar.scrollbarThickness;
  // if (metrics.toolboxPosition == Blockly.TOOLBOX_AT_LEFT) {
  //   this.left_ += metrics.flyoutWidth;
  //   if (this.workspace_.toolbox_) {
  //     this.left_ += metrics.absoluteLeft;
  //   }
  // }
  //   } else {
  //     this.left_ = metrics.viewWidth + metrics.absoluteLeft -
  //         this.WIDTH_ - this.MARGIN_SIDE_ - Blockly.Scrollbar.scrollbarThickness;

  //     if (metrics.toolboxPosition == Blockly.TOOLBOX_AT_RIGHT) {
  //       this.left_ -= metrics.flyoutWidth;
  //     }
  //   }
  this.top_ = metrics.viewHeight + metrics.absoluteTop -
    (this.BODY_HEIGHT_ + this.LID_HEIGHT_) - this.bottom_;

  if (metrics.toolboxPosition == Blockly.TOOLBOX_AT_BOTTOM) {
    this.top_ -= metrics.flyoutHeight;
  }

  if (this.left_ && this.top_) {
    this.svgGroup_.setAttribute('transform', 'translate(' + this.left_ + ',' + this.top_ + ')');
  }
};

/**
 * Return the deletion rectangle for this trash can.
 * @return {goog.math.Rect} Rectangle in which to delete.
 */

Blockly.Trashcan.prototype.getClientRect = function () {
  if (!this.svgGroup_) {
    return null;
  }

  var trashRect = this.svgGroup_.getBoundingClientRect();
  var top = trashRect.top;// + this.SPRITE_TOP_ - this.MARGIN_HOTSPOT_;
  var bottom = top + this.LID_HEIGHT_ + this.BODY_HEIGHT_ +
    7 * this.MARGIN_HOTSPOT_;
  var left = trashRect.left + this.SPRITE_LEFT_ - this.MARGIN_HOTSPOT_;
  var right = left + this.WIDTH_ + 2 * this.MARGIN_HOTSPOT_;
  return new Blockly.utils.Rect(top, bottom, left, right);
};

/**
 * Flip the lid open or shut.
 * @param {boolean} state True if open.
 * @private
 */
Blockly.Trashcan.prototype.setOpen_ = function (state) {

  if (!this.isHidden) this.svgGroup_.style.opacity = state ? .7 : .5;
  if (this.isOpen == state) {
    return;
  }
  clearTimeout(this.lidTask_);
  this.isOpen = state;
  this.animateLid_();
};

/**
 * Rotate the lid open or closed by one step.  Then wait and recurse.
 * @private
 */
var isFirst = true;
Blockly.Trashcan.prototype.animateLid_ = function () {
  if (isFirst) {
    this.svgGroup_.style.opacity = 0;
    isFirst = false;
    return;
  }
  this.lidOpen_ += this.isOpen ? 0.2 : -0.2;

  // Linear interpolation between 0.4 and 0.8.
  // var opacity = 0.4 + this.lidOpen_;// * (0.8 - 0.4);
  var opacity = 0.2 + this.lidOpen_;
  this.svgGroup_.style.opacity = opacity;
  // if (this.lidOpen_ > 0 && this.lidOpen_z < 1) {
  //   this.lidTask_ = setTimeout(this.animateLid_.bind(this), 20);
  // }
};

/**
 * Flip the lid shut.
 * Called externally after a drag.
 */
Blockly.Trashcan.prototype.close = function () {
  this.setOpen_(false);
  this.hide(true);
};


/**
 * Inspect the contents of the trash.
 */
Blockly.Trashcan.prototype.click = function () {
  var dx = this.workspace_.startScrollX - this.workspace_.scrollX;
  var dy = this.workspace_.startScrollY - this.workspace_.scrollY;
  if (Math.sqrt(dx * dx + dy * dy) > Blockly.DRAG_RADIUS) {
    return;
  }
};

Blockly.Trashcan.prototype.hide = function (hide) {
  this.isHidden = hide;
  this.svgGroup_.style.opacity = hide ? 0 : .5;
};


/**
 * Start dragging a block.  This includes moving it to the drag surface.
 * @param {!Blockly.utils.Coordinate} currentDragDeltaXY How far the pointer has
 *     moved from the position at mouse down, in pixel units.
 * @param {boolean} healStack Whether or not to heal the stack after
 *     disconnecting.
 * @package
 */
Blockly.BlockDragger.prototype.startBlockDrag = function (currentDragDeltaXY,
  healStack) {
  if (!Blockly.Events.getGroup()) {
    Blockly.Events.setGroup(true);
  }

  if (!this.workspace_.isMutator) {
    this.workspace_.trashcan.hide(false);
  }

  // Mutators don't have the same type of z-ordering as the normal workspace
  // during a drag.  They have to rely on the order of the blocks in the SVG.
  // For performance reasons that usually happens at the end of a drag,
  // but do it at the beginning for mutators.
  if (this.workspace_.isMutator) {
    this.draggingBlock_.bringToFront();
  }

  // During a drag there may be a lot of rerenders, but not field changes.
  // Turn the cache on so we don't do spurious remeasures during the drag.
  Blockly.Field.startCache();
  this.workspace_.setResizesEnabled(false);
  Blockly.blockAnimations.disconnectUiStop();

  if (this.draggingBlock_.getParent() ||
    (healStack && this.draggingBlock_.nextConnection &&
      this.draggingBlock_.nextConnection.targetBlock())) {
    this.draggingBlock_.unplug(healStack);
    var delta = this.pixelsToWorkspaceUnits_(currentDragDeltaXY);
    var newLoc = Blockly.utils.Coordinate.sum(this.startXY_, delta);

    this.draggingBlock_.translate(newLoc.x, newLoc.y);
    Blockly.blockAnimations.disconnectUiEffect(this.draggingBlock_);
  }
  this.draggingBlock_.setDragging(true);
  // For future consideration: we may be able to put moveToDragSurface inside
  // the block dragger, which would also let the block not track the block drag
  // surface.
  this.draggingBlock_.moveToDragSurface_();

  var toolbox = this.workspace_.getToolbox();
  if (toolbox) {
    var style = this.draggingBlock_.isDeletable() ? 'blocklyToolboxDelete' :
      'blocklyToolboxGrab';
    toolbox.addStyle(style);
  }
};


/**
* Finish a block drag and put the block back on the workspace.
* @param {!Event} e The mouseup/touchend event.
* @param {!Blockly.utils.Coordinate} currentDragDeltaXY How far the pointer has
*     moved from the position at the start of the drag, in pixel units.
* @package
*/
Blockly.BlockDragger.prototype.endBlockDrag = function (e, currentDragDeltaXY) {
  // Make sure internal state is fresh.
  this.dragBlock(e, currentDragDeltaXY);
  this.dragIconData_ = [];

  if (!this.workspace_.isMutator) {
    this.workspace_.trashcan.hide(true);
  }

  Blockly.Field.stopCache();

  Blockly.blockAnimations.disconnectUiStop();

  var delta = this.pixelsToWorkspaceUnits_(currentDragDeltaXY);
  var newLoc = Blockly.utils.Coordinate.sum(this.startXY_, delta);
  this.draggingBlock_.moveOffDragSurface_(newLoc);

  var deleted = this.maybeDeleteBlock_();
  if (!deleted) {
    // These are expensive and don't need to be done if we're deleting.
    this.draggingBlock_.moveConnections_(delta.x, delta.y);
    this.draggingBlock_.setDragging(false);
    this.fireMoveEvent_();
    if (this.draggedConnectionManager_.wouldConnectBlock()) {
      // Applying connections also rerenders the relevant blocks.
      this.draggedConnectionManager_.applyConnections();
    } else {
      this.draggingBlock_.render();
    }
    this.draggingBlock_.scheduleSnapAndBump();
  }
  this.workspace_.setResizesEnabled(true);

  var toolbox = this.workspace_.getToolbox();
  if (toolbox) {
    var style = this.draggingBlock_.isDeletable() ? 'blocklyToolboxDelete' :
      'blocklyToolboxGrab';
    toolbox.removeStyle(style);
  }
  Blockly.Events.setGroup(false);
};

