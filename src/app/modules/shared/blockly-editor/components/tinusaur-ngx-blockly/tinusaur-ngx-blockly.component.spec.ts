/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TinusaurNgxBlocklyComponent } from './tinusaur-ngx-blockly.component';

describe('TinusaurNgxBlocklyComponent', () => {
  let component: TinusaurNgxBlocklyComponent;
  let fixture: ComponentFixture<TinusaurNgxBlocklyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinusaurNgxBlocklyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinusaurNgxBlocklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
