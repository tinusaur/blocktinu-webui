import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerminalComponent } from './terminal.component';
import { NgTerminalModule } from 'ng-terminal';

@NgModule({
  imports: [
    CommonModule,
    NgTerminalModule
  ],
  declarations: [
    TerminalComponent
  ],
  exports: [
    TerminalComponent
  ]
})
export class TerminalModule { }
