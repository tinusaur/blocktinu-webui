import { Component, OnInit, AfterViewInit, ViewChild, EventEmitter, Output, OnDestroy } from '@angular/core';
import { NgTerminal } from 'ng-terminal';
import { terminalHelpText } from './config';

import { SearchAddon } from 'xterm-addon-search';
import { KeyCodes } from './keycodes';
import { Subscription } from 'rxjs';


const ANSI_COLORS: any = {
  red: `\u001b[31m`,
  green: `\u001b[32m`,
  blue: `\u001b[34m`,
  nc: `\u001b[0m`,
};

// this.terminal.echo('\u001b[34mHello\u001b[39m World\u001b[31m!\u001b[39m');

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent implements AfterViewInit, OnDestroy {
  @ViewChild('terminal', { static: true }) child: NgTerminal;
  private PS1 = 'blocktinu ~ ';
  private accumulatedCommand = '';

  private blocked = false;
  searchAddon = new SearchAddon();

  @Output() onCommand = new EventEmitter<string>();

  constructor() { }

  private invalidate() {
    this.accumulatedCommand = '';
  }

  public echo(str: string) {
    this.write(`${str}\r\n`);
    if (!this.blocked) {
      this.write(this.PS1);
    }
  }

  public colorizeHeader(header: string): string {
    return `${ANSI_COLORS.blue}${header}${ANSI_COLORS.nc}`;
  }


  public error(str: string) {
    this.echo(`${ANSI_COLORS.red}${str}${ANSI_COLORS.nc}`);
  }

  public success(str: string) {
    this.echo(`${ANSI_COLORS.green}${str}${ANSI_COLORS.nc}`);
  }


  private write(str: string) {
    this.child.write(str);
  }


  private search(query: string) {
    this.searchAddon.findNext('foo');
  }

  private keyEventInputSubscription: Subscription;
  ngOnDestroy() {
    if (this.keyEventInputSubscription) this.keyEventInputSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.invalidate();
    this.child.underlying.loadAddon(this.searchAddon);

    this.child.underlying.setOption('fontSize', 13);
    this.child.underlying.setOption("theme", {
      foreground: '#000',
      background: '#fff',
      cursor: '#000',
      selection: 'rgba(255, 255, 255, 0.5)',
      black: '#000000',
      red: '#C96169',
      brightRed: '#e06c75',
      green: '#699967',
      brightGreen: '#699967',
      brightYellow: '#EDDC96',
      yellow: '#EDDC96',
      magenta: '#e39ef7',
      brightMagenta: '#e39ef7',
      cyan: '#4CA2AC',
      brightBlue: '#4CA2AC',
      brightCyan: '#4CA2AC',
      blue: '#3C8189',
      white: '#d0d0d0',
      brightBlack: '#808080',
      brightWhite: '#ffffff'
    });

    this.reset();

    this.keyEventInputSubscription = this.child.keyEventInput.subscribe(e => {
      if (this.blocked) {
        return;
      }

      const ev = e.domEvent;
      const printable = !ev.altKey && !ev.ctrlKey && !ev.metaKey;

      if (ev.keyCode === KeyCodes.enter) {
        this.write(`\r\n`);
        // execute
        this.executeCommand(this.accumulatedCommand);
        this.invalidate();
      } else if (ev.keyCode === KeyCodes.backspace) {
        // Do not delete the prompt
        if (this.child.underlying.buffer.cursorX > 2) {
          this.write('\b \b');
          this.accumulatedCommand = this.accumulatedCommand.slice(0, -1);
        }
      } else if (ev.keyCode === KeyCodes.uparrow) {

      } else if (ev.keyCode === KeyCodes.downarrow) {

      } else if (ev.keyCode === KeyCodes.leftarrow) {

      } else if (ev.keyCode === KeyCodes.rightarrow) {

      } else if (printable) {
        this.write(e.key);
        this.accumulatedCommand += e.key;
      } else if (ev.keyCode === KeyCodes.l) {
        this.executeCommand('clear');
      } else if (ev.keyCode === KeyCodes.b) {
        this.executeCommand('build');
        this.invalidate();
      }
    })
  }

  private parseCommand(cmd: string): { command: string, commandArguments: Array<string> } {
    const expression = cmd.split(' ');
    const command = expression.shift();
    const commandArguments = expression;
    return { command, commandArguments };
  }

  private executeCommand(cmd: string): void {
    const parsedExpression = this.parseCommand(cmd);

    switch (parsedExpression.command) {
      case 'compile':
        this.block();
        this.onCommand.emit('compile');
        this.resume();
        break;
      case 'download':
        this.block();
        // this.echo('downloading...');
        this.onCommand.emit('download');
        this.resume();
        break;
      case 'save':
        this.block();
        this.onCommand.emit('save');
        this.resume();
        break;
      case 'build':
        this.block();
        this.onCommand.emit('build');
        // this.resume();
        break;
      case 'clear':
        this.reset();
        break;

      case 'help':
        this.echo(terminalHelpText);
        break;

      default:
        this.echo(`Unknown command: ${parsedExpression.command}`);
        break;
    }
  }

  private reset() {
    this.child.underlying.reset();
    this.write('Welcome to Blocktinu terminal\r\n');
    this.write(this.PS1);
  }
  public block() {
    this.blocked = true;
  }
  public resume() {
    if (this.blocked) {
      this.write(this.PS1);
    }
    this.blocked = false;
  }

}
