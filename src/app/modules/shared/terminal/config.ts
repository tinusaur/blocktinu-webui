
export const terminalHelpText = `You can use following commands:\r
'compile' - to compile current C code\r
'build' - to build current project\r
'save' - to save current code\r
'download' - to download current JSON code\r
'clear' - clear console output\r
'help' - to show this msg\r
Key bindings:\r
'CTRL + L' - clear console output\r
`;
