import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodeEditorComponent } from './code-editor.component';
import { NzIconModule, NzSpinModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,
    NzIconModule,
    NzSpinModule
  ],
  declarations: [CodeEditorComponent],
  exports: [
    CodeEditorComponent
  ]
})
export class CodeEditorModule { }
