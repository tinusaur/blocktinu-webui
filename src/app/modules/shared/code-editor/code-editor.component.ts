import { Component, OnInit, ViewChild, Input, SimpleChanges, OnChanges, Output, EventEmitter, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import * as ace from 'ace-builds';
import 'ace-builds/src-noconflict/mode-c_cpp';
import 'ace-builds/src-noconflict/theme-chrome';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-beautify';

export class Marker {
  startLineNumber: number;
  startColumn: number;
  endLineNumber: number;
  endColumn: number;
  message: string;
  severity: 'error' | 'warning';
}

const THEME = 'ace/theme/chrome';
const LANG = 'ace/mode/c_cpp';

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss']
})
export class CodeEditorComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  @Input() readonly: boolean = false;
  @Input() code: string;
  @Input() title: string = 'Blocktinu Source Code';

  @Output() onCode: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('aceEditor', { static: false }) codeEditorElmRef: ElementRef;
  private codeEditor: ace.Ace.Editor;
  private editorBeautify;

  private aceEditor$ = new BehaviorSubject<ace.Ace.Editor>(null);

  loading = false;

  constructor() { }
  ngOnInit() { }

  ngOnDestroy(): void {
    if (this.aceEditor$) this.aceEditor$.complete();
  }

  ngAfterViewInit(): void {
    // init ace editor
    ace.require('ace/ext/language_tools');
    const element = this.codeEditorElmRef.nativeElement;
    const editorOptions = this.getEditorOptions();
    this.codeEditor = ace.edit(element, editorOptions);
    this.codeEditor.setOptions({
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true
    });
    this.codeEditor.setTheme(THEME);
    this.codeEditor.getSession().setMode(LANG);
    this.codeEditor.setShowFoldWidgets(true);
    // emit changes
    this.codeEditor.on("change", _delta => {
      this.onCodeChange(this.getContent());
    });
    // hold reference to beautify extension
    this.editorBeautify = ace.require('ace/ext/beautify');

    this.aceEditor$.next(this.codeEditor);

    // set editor options and value
    this.codeEditor.setReadOnly(this.readonly);
    if (this.code) this.setValue(this.code);
  }

  /**
   * Set value and move user's cursor to the end.
   */
  private setValue(value: string) {
    if (this.codeEditor) this.codeEditor.setValue(value, 1);
  }

  // missing propery on EditorOptions 'enableBasicAutocompletion' so this is a wolkaround still using ts
  private getEditorOptions(): Partial<ace.Ace.EditorOptions> & { enableBasicAutocompletion?: boolean; } {
    const basicEditorOptions: Partial<ace.Ace.EditorOptions> = {
      highlightActiveLine: true
    };
    const extraEditorOptions = { enableBasicAutocompletion: true };
    return Object.assign(basicEditorOptions, extraEditorOptions);
  }

  /**
   * current editor's content.
   */
  public getContent() {
    if (this.codeEditor) {
      return this.codeEditor.getValue();
    }
  }

  /**
   * beautify the editor content, rely on Ace Beautify extension.
   */
  public beautifyContent(): void {
    if (this.codeEditor && this.editorBeautify) {
      const session = this.codeEditor.getSession();
      this.editorBeautify.beautify(session);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['readonly'] && changes['readonly'].previousValue != changes['readonly'].currentValue) {
      if (this.codeEditor) this.codeEditor.setReadOnly(changes['readonly'].currentValue);
    }

    if (this.readonly && changes['code'] && changes['code'].previousValue != changes['code'].currentValue) {
      if (this.codeEditor) this.setValue(changes['code'].currentValue);
    }

  }

  onCodeChange(code: string) {
    if (!this.readonly) {
      this.onCode.emit(code);
      this.setMarkers([]);
    }
  }

  /*
   * Format code
   */
  formatCode() {
    this.beautifyContent();
  }

  /*
   * Set code editor markers
   */
  setMarkers(markers: Array<Marker>) {
    if (this.codeEditor) {

      // rm old markers
      const oldMarkers = this.codeEditor.getSession().getMarkers(true);
      for (let key in oldMarkers) {
        this.codeEditor.getSession().removeMarker(parseInt(key));
      };

      // set new markers
      markers.forEach(marker => {
        this.codeEditor.getSession().addMarker(
          new ace.Range(
            marker.startLineNumber - 1,
            marker.startColumn,
            marker.endLineNumber - 1,
            marker.endColumn
          ), marker.severity, "fullLine", true)
      });
    }
  }
}
