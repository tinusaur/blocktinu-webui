import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomsListComponent } from './components/classrooms-list/classrooms-list.component';
import { ClassroomsDetailComponent } from './components/classrooms-detail/classrooms-detail.component';
import { ClassroomsRouting } from './classrooms-routing.module';
import {
  NzMenuModule, NzInputModule, NzGridModule, NzCardModule,
  NzButtonModule, NzIconModule, NzDividerModule, NzListModule,
  NzToolTipModule,
  NzDescriptionsModule, NzTypographyModule, NzSelectModule, NzPaginationModule, NzFormModule, NzAvatarModule, NzResultModule, NzPopconfirmModule, NzModalModule, NzTabsModule
} from 'ng-zorro-antd';

import { ClassroomsListPageComponent } from './components/classrooms-list-page/classrooms-list-page.component';
import { ClassroomsFiltersComponent } from './components/classrooms-filters/classrooms-filters.component';
import { ClassroomsListItemComponent } from './components/classrooms-list-item/classrooms-list-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateClassroomComponent } from './components/create-classroom/create-classroom.component';
import { ImagesSelectorModule } from '../shared/images-selector/images-selector.module';
import { UsersSelectorModule } from '../shared/users-selector/users-selector.module';
import { ProjectsModule } from '../projects/projects.module';
import { ClassroomsComponent } from './components/classrooms/classrooms.component';

@NgModule({
  imports: [
    CommonModule,
    ClassroomsRouting,
    FormsModule,
    ReactiveFormsModule,
    ImagesSelectorModule,
    UsersSelectorModule,
    NzMenuModule,
    NzInputModule,
    NzGridModule,
    NzCardModule,
    NzButtonModule,
    NzIconModule,
    NzDividerModule,
    NzListModule,
    NzToolTipModule,
    NzTypographyModule,
    NzSelectModule,
    NzDescriptionsModule,
    NzFormModule,
    NzPaginationModule,
    NzAvatarModule,
    NzResultModule,
    NzModalModule,
    ProjectsModule,
    NzPopconfirmModule,
    NzTabsModule
  ],
  declarations: [
    ClassroomsListPageComponent,
    ClassroomsComponent,
    ClassroomsListComponent,
    ClassroomsListItemComponent,
    ClassroomsDetailComponent,
    CreateClassroomComponent,
    ClassroomsFiltersComponent
  ],
  exports: [
    ClassroomsComponent
  ]
})
export class ClassroomsModule { }
