import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomsListPageComponent } from './components/classrooms-list-page/classrooms-list-page.component';
import { ClassroomsDetailComponent } from './components/classrooms-detail/classrooms-detail.component';
import { ClassroomsResolverService } from './services/classrooms-resolver.service';
import { ClassroomDetailsResolverService } from './services/classroom-details-resolver.service';
import { CreateClassroomComponent } from './components/create-classroom/create-classroom.component';
import { ClassroomsComponent } from './components/classrooms/classrooms.component';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  {
    path: 'create',
    component: CreateClassroomComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'details/:classroomId',
    component: ClassroomsDetailComponent,
    resolve: {
      classroom: ClassroomDetailsResolverService,
    },
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    canActivate: [AuthGuard],
  },
  {
    path: '',
    component: ClassroomsListPageComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', pathMatch: 'exact', redirectTo: 'my' },
      {
        path: 'my',
        component: ClassroomsComponent,
        resolve: {
          classrooms: ClassroomsResolverService,
        },
        data: {
          only_current_user_classrooms: true,
          // id_in_members_in_parent_parameter
        },
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassroomsRouting { }
