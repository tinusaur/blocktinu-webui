/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClassroomsResolverService } from './classrooms-resolver.service';

describe('Service: ClassroomsResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassroomsResolverService]
    });
  });

  it('should ...', inject([ClassroomsResolverService], (service: ClassroomsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
