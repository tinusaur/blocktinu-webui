import { Injectable } from '@angular/core';
import { ClassroomsService } from './classrooms.service';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY, of, throwError } from 'rxjs';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { CredentialsService } from '../../auth/credentials.service';

@Injectable({
  providedIn: 'root'
})
export class ClassroomsResolverService implements Resolve<PaginatedApiResponse<Classroom>> {

  constructor(
    private classroomsService: ClassroomsService,
    private credentialsService: CredentialsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<PaginatedApiResponse<Classroom>> | Observable<never> {

    const query = route.queryParamMap.get('q');
    let ownerId = route.queryParamMap.get('owner_id');
    let id_in_members = route.queryParamMap.get('id_in_members');
    const page = route.queryParamMap.get('page');
    const page_size = route.queryParamMap.get('page_size');

    if ('only_current_user_classrooms' in route.data && route.data.only_current_user_classrooms && this.credentialsService.isLoggedIn && this.credentialsService.currentUser) {
      const only_current_user_classrooms = route.data['only_current_user_classrooms'];
      if (only_current_user_classrooms) id_in_members = String(this.credentialsService.currentUser.id)
    } else if ('id_in_members_in_parent_parameter' in route.data && route.data.id_in_members_in_parent_parameter) {
      // e.g. /users/:userId/classrooms
      id_in_members = route.parent.paramMap.get(route.data.id_in_members_in_parent_parameter);
    }

    let filters = [];

    if (query) {
      filters.push({ param: 'search', value: query });
    }

    if (ownerId) {
      filters.push({ param: 'owner_id', value: ownerId });
    }

    if (id_in_members) {
      filters.push({ param: 'id_in_members', value: id_in_members });
    }

    if (page) {
      filters.push({ param: 'page', value: page });
    }

    if (page_size) {
      filters.push({ param: 'page_size', value: page_size });
    }

    return this.classroomsService.getByFilters(filters).pipe(
      take(1),
      mergeMap((classrooms: PaginatedApiResponse<Classroom>) => {
        if (classrooms) {
          return of(classrooms);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
