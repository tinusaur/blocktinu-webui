/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClassroomDetailsResolverService } from './classroom-details-resolver.service';

describe('Service: ClassroomDetailsResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassroomDetailsResolverService]
    });
  });

  it('should ...', inject([ClassroomDetailsResolverService], (service: ClassroomDetailsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
