import { Injectable } from '@angular/core';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { Observable, throwError } from 'rxjs';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { CustomEncoder } from 'src/app/core/services/custom.encoder';
import { HttpParams, HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, first } from 'rxjs/operators';
import { ClassroomApiRequest } from 'src/app/core/models/api-request/classroom-api-request';
import { UploadFile } from 'ng-zorro-antd';
import { User } from 'src/app/core/models/User.model';

@Injectable({
  providedIn: 'root'
})
export class ClassroomsService {
  private encoder = new CustomEncoder();
  private baseUrl = `${environment.API_URL}/classrooms`;

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService) {
  }

  public getByFilters(filters: Array<{ param: string, value: string }> = []) {
    let params = new HttpParams({ encoder: new CustomEncoder() });

    for (let item of filters) {
      params = params.append(item.param, item.value);
    }

    const options =
      { params: params };

    return this.http.get<PaginatedApiResponse<Classroom>>(`${this.baseUrl}/`, options)
      .pipe(
        first(),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return throwError(error);
        })
      );
  }

  public get(page: string, page_size: string, query: string = null, id_in_members: string = null): Observable<PaginatedApiResponse<Classroom>> {
    let params = new HttpParams({ encoder: this.encoder }).set('page', page).set('page_size', page_size);

    if (query) {
      params = params.set('search', query);
    }
    if (id_in_members) {
      params = params.set('id_in_members', id_in_members);
    }

    const options =
      { params: params };

    return this.http.get<PaginatedApiResponse<Classroom>>(`${this.baseUrl}/`, options).pipe(first());
  }

  public getNext(next: string): Observable<PaginatedApiResponse<Classroom>> {
    return this.http.get<PaginatedApiResponse<Classroom>>(next).pipe(first());
  }


  public getById(id: number | string): Observable<Classroom> {
    return this.http.get<Classroom>(`${this.baseUrl}/${id}/`).pipe(first());
  }

  public patch(id: number | string, patchObject: object): Observable<Classroom> {
    return this.http.patch<Classroom>(`${this.baseUrl}/${id}/`, patchObject).pipe(first());
  }

  public delete(id: string | number): Observable<null> {
    return this.http.delete<null>(`${this.baseUrl}/${id}/`).pipe(first());
  }

  public addMembers(classroom: Classroom, newMembers: Array<User>): Observable<Classroom> {
    let mergedMembers = [...classroom.members, ...newMembers];
    const mergedUrls = mergedMembers.map((v) => {
      if (v instanceof String) {
        return v;
      } else {
        return (v as User).url;
      }
    });
    return this.patch(classroom.id, { members: mergedUrls }).pipe(first());
  }

  public removeMember(classroom: Classroom, oldMember: User): Observable<Classroom> {
    let newMembers = classroom.members.filter((u: User) => u.id != oldMember.id);

    const newUrls = newMembers.map((v) => {
      if (v instanceof String) {
        return v;
      } else {
        return (v as User).url;
      }
    });

    return this.patch(classroom.id, { members: newUrls }).pipe(first());
  }

  private generateNewFormData(classroom: ClassroomApiRequest): FormData {
    const formData = new FormData();

    for (var key in classroom) {
      if (key == 'members') {
        // members
        for (let t of classroom.members) {
          formData.append('members', t);
        }
      } else if (key == 'images') {
        // images
        classroom.images.forEach((value: UploadFile, index: number, array) => {
          if (!value.url && value.originFileObj) {
            formData.append(`image_${index}`, value.originFileObj);
          }
        });

      } else {
        formData.append(key, classroom[key]);
      }
    }

    return formData;
  }


  // TODO: handle unsubsription
  public create(classroom: ClassroomApiRequest): Observable<HttpEvent<{}>> {
    const formData = this.generateNewFormData(classroom);

    const url: string = `${this.baseUrl}/`;

    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      withCredentials: true,
    });

    return this.http.request(req).pipe(
      catchError(error => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      })
    );
  }

  public exitClassroom(classroom: Classroom): Observable<{ removed: boolean }> {
    return this.http.post<{ removed: boolean }>(`${this.baseUrl}/${classroom.id}/exit/`, {}).pipe(first());
  }

}
