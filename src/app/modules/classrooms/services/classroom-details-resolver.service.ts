import { Injectable } from '@angular/core';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ClassroomsService } from './classrooms.service';
import { Observable, of, EMPTY, throwError } from 'rxjs';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ClassroomDetailsResolverService implements Resolve<Classroom> {


  constructor(private classroomsService: ClassroomsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Classroom> | Observable<never> {

    const id = route.paramMap.get('classroomId');

    return this.classroomsService.getById(id).pipe(
      take(1),
      mergeMap((classrooms: Classroom) => {
        if (classrooms) {
          return of(classrooms);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
