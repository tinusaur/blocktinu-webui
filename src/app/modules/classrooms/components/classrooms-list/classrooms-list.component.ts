import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { UserRole } from 'src/app/core/models/User.model';

@Component({
  selector: 'app-classrooms-list',
  templateUrl: './classrooms-list.component.html',
  styleUrls: ['./classrooms-list.component.scss']
})
export class ClassroomsListComponent implements OnInit, OnChanges {

  @Input() classrooms: PaginatedApiResponse<Classroom>;
  @Input() showCreateNewClassroomButton: boolean = true;
  @Input() loading: boolean = false;
  @Output() onPageIndexChange = new EventEmitter<number>();
  emptyClassroomListHolder = new Classroom();
  constructor(
    public credentialsService: CredentialsService
  ) {
    this.emptyClassroomListHolder.title = "Create New Classroom";
    this.emptyClassroomListHolder.is_new_classroom_button_holder = true;
  }

  ngOnInit() {
  }

  onPageChange(newPageIndex: number) {
    this.onPageIndexChange.emit(newPageIndex);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.showCreateNewClassroomButton &&
      changes['classrooms'] &&
      changes.classrooms.previousValue != changes.classrooms.currentValue &&
      this.credentialsService.currentUser &&
      (this.credentialsService.currentUser.role == UserRole.STAFF || this.credentialsService.currentUser.role == UserRole.TEACHER)) {
      if (this.classrooms && this.classrooms.results) this.classrooms.results.unshift(this.emptyClassroomListHolder);
    }
  }
}
