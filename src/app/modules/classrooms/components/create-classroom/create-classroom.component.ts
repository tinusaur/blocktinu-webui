import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';
import { ClassroomsService } from '../../services/classrooms.service';
import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ClassroomApiRequest } from 'src/app/core/models/api-request/classroom-api-request';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';

@Component({
  selector: 'app-create-classroom',
  templateUrl: './create-classroom.component.html',
  styleUrls: ['./create-classroom.component.scss']
})
export class CreateClassroomComponent implements OnInit {

  classroomForm: FormGroup;
  loading: boolean = false;
  percent: number = 0;

  maxImagesLength: number = 1;

  constructor(
    public credentialsService: CredentialsService,
    private classroomsService: ClassroomsService,
    private nzMessageService: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.classroomForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(900)]],
      members: [[], [Validators.required, Validators.minLength(1)]],
      // image_1: File, image_2: File, ...
      images: [[], [Validators.required, Validators.minLength(1), Validators.maxLength(this.maxImagesLength)]],
    });
  }

  ngOnInit() {

  }

  submitForm(value: any): void {
    for (const key in this.classroomForm.controls) {
      this.classroomForm.controls[key].markAsDirty();
      this.classroomForm.controls[key].updateValueAndValidity();
    }

    if (this.classroomForm.valid) {
      // create new classroom
      this.loading = true;
      let req: ClassroomApiRequest = { ...this.classroomForm.value };
      req.members = this.classroomForm.controls["members"].value.map(v => v.url);
      req.members.push(this.credentialsService.currentUser.url);
      console.log(req);
      this.classroomsService.create(req)
        .subscribe(
          (event: HttpEvent<{}>) => {
            if (event.type === HttpEventType.UploadProgress) {
              if (event.total! > 0) {
                this.percent = (event.loaded / event.total!) * 100;
              }
            } else if (event instanceof HttpResponse) {
              // uploaded
              this.loading = false;
              const newService = event.body;
              const id = newService['id'];
              this.nzMessageService.success(`Successfully created classroom ${id}`)
              this.router.navigate(['..', 'details', id], {
                relativeTo: this.route
              });
            }
          },
          err => {
            // fail
            this.nzMessageService.error("Error creating classroom");
            this.loading = false;
          });
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.classroomForm.reset();
    for (const key in this.classroomForm.controls) {
      this.classroomForm.controls[key].markAsPristine();
      this.classroomForm.controls[key].updateValueAndValidity();
    }
  }


}
