import { Component, OnInit } from '@angular/core';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';

@Component({
  selector: 'app-classrooms',
  templateUrl: './classrooms.component.html',
  styleUrls: ['./classrooms.component.scss']
})
export class ClassroomsComponent implements OnInit {

  paginatedClassrooms: PaginatedApiResponse<Classroom> = new PaginatedApiResponse<Classroom>();
  page;
  page_size;
  loading: boolean = false;
  hideCreateButton: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public tds: TargetDeviceService
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(params => {
      this.page_size = +params.get('page_size') || this.page_size;
      this.page = +params.get('page') || this.page;
    });

    // unsubscription is handled by ActivatedRoute
    this.route.data
      .subscribe((data: { classrooms: PaginatedApiResponse<Classroom>, hide_create_button: boolean }) => {
        if (data.classrooms) {
          this.paginatedClassrooms = data.classrooms;
          this.loading = false;
        }
        if (data.hide_create_button) {
          this.hideCreateButton = true;
        }
      });
  }

  search(q: string) {
    this.loading = true;
    const queryParams: Params = { q };
    this.updateQueryParams(queryParams);
  }

  loadDataForPage(pi: number): void {
    this.loading = true;
    this.page = pi;
    const queryParams: Params = { page: pi, page_size: this.page_size };
    this.updateQueryParams(queryParams);
  }

  private updateQueryParams(queryParams: Params) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
        queryParamsHandling: "merge", // remove to replace all query params by provided
      });
  }


}
