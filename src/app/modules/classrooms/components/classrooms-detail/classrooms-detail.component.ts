import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { ClassroomsService } from '../../services/classrooms.service';
import { User } from 'src/app/core/models/User.model';
import { UserService } from 'src/app/core/services/user.service';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { ProjectsService } from 'src/app/modules/projects/services/projects.service';
import { Project } from 'src/app/core/models/Project.model';

@Component({
  selector: 'app-classrooms-detail',
  templateUrl: './classrooms-detail.component.html',
  styleUrls: ['./classrooms-detail.component.scss']
})
export class ClassroomsDetailComponent implements OnInit {

  classroom: Classroom;
  readonly: boolean = true;
  loading: boolean = false;

  projects: PaginatedApiResponse<Project>;

  showAddNewMemberModal: boolean = false;

  constructor(
    public credentialsService: CredentialsService,
    private classroomsService: ClassroomsService,
    private projectsService: ProjectsService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    // ubsub handled by activatedRoute
    this.route.data.subscribe((data: { classroom: Classroom }) => {
      this.classroom = data.classroom;

      if (this.classroom && this.credentialsService.currentUser && this.credentialsService.currentUser.id === this.classroom.owner_id) {
        this.readonly = false;
      }
      this.initClassroomInfo(this.classroom);
    });

  }

  public canLeave(user: User): boolean {
    if (!this.classroom || !user) {
      return false;
    }
    return user.role != 3 && this.classroom.owner_id != user.id && this.classroom.members.filter((v: User) => v.id == user.id).length > 0;
  }

  public exitClassroom(classroom: Classroom): void {
    this.loading = true;
    this.classroomsService.exitClassroom(classroom)
      .subscribe(r => {
        this.classroom.members = this.classroom.members.filter((v: User) => v.id != this.credentialsService.currentUser.id);
        this.loading = false;
      });
  }

  public getClassroomMembersIds(classroom: Classroom): Array<number> {
    if (!classroom) return [];
    return classroom.members.map((v: User) => Number(v.id));
  }


  public deleteClassroom(classroom: Classroom): void {
    this.loading = true;
    this.classroomsService.delete(classroom.id)
      .subscribe(r => {
        this.router.navigate(['/', 'classrooms']);
      });
  }


  patchClassroom(patchObj: any): void {
    this.loading = true;
    // ubsub handled by ClassroomsService
    this.classroomsService.patch(this.classroom.id, patchObj)
      .subscribe(classroom => {
        this.classroom = classroom;
        this.loading = false;
      });
  }

  private initClassroomInfo(classroom: Classroom) {
    if (classroom && classroom.id) {
      this.classroom = classroom;
      if (!this.credentialsService.currentUser) {
        this.projectsTabIndex = 1;
      }
      this.loadProjects();

    }
    this.loading = false;
  }

  private loadProjects() {
    // subscription handled by ProjectsService
    const ownerId = this.projectsTabIndex == 0 && this.credentialsService.currentUser ? this.credentialsService.currentUser.id : null;
    this.projectsService.getByClassroomIdAndOwnerId(this.classroom.id, ownerId)
      .subscribe(projects => {
        this.projects = projects;
      });
  }

  //
  // Members management
  //
  private newMembers: Array<User> = [];
  onUserSelectorChange(m: Array<User>) {
    this.newMembers = m;
  }

  public addMembersToClassroom(classroom: Classroom) {
    if (this.newMembers.length > 0) {
      this.classroomsService.addMembers(classroom, this.newMembers)
        .subscribe(g => {
          this.classroom = g;
          this.newMembers = [];
          this.showAddNewMemberModal = false;
        });
    }
  }

  public removeMemberFromClassroom(classroom: Classroom, member: User) {
    this.classroomsService.removeMember(classroom, member)
      .subscribe(g => {
        this.classroom = g;
      })
  }

  projectsTabIndex = 0;
  public onProjectsTabChange(index: number) {
    if (this.projectsTabIndex != index) {
      this.projectsTabIndex = index;
      this.loadProjects();
    }
  }
}
