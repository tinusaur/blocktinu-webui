import { Component, OnInit, Input } from '@angular/core';
import { Classroom } from 'src/app/core/models/Classroom.model';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { User } from 'src/app/core/models/User.model';

@Component({
  selector: 'app-classrooms-list-item',
  templateUrl: './classrooms-list-item.component.html',
  styleUrls: ['./classrooms-list-item.component.scss']
})
export class ClassroomsListItemComponent implements OnInit {

  @Input() classroom: Classroom;
  constructor(
    public credentialsService: CredentialsService
  ) { }

  ngOnInit() {
  }


  public getCurrentUserRole(user: User): string {
    if (!this.classroom || !user) {
      return null;
    }
    if(this.classroom.owner_id == user.id) {
      return 'Owner';
    }
    if(this.classroom.members.filter(v => v == user.url).length > 0) {
      return 'Member';
    }
  }
}
