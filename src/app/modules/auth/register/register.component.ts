import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { RegisterApiRequest } from '../models/register-api-request.model';
import { LoginApiRequest } from '../models/login-api-request.model';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

	constructor(
		private fb: FormBuilder,
		public authService: AuthService,
		public userService: UserService,
		public router: Router) {
	}

	loading: boolean = false;
	validateForm: FormGroup;

	submitForm(): void {
		this.loading = true;
		for (const i in this.validateForm.controls) {
			this.validateForm.controls[i].markAsDirty();
			this.validateForm.controls[i].updateValueAndValidity();
		}

		if (this.validateForm.valid) {
			const rar = new RegisterApiRequest();

      rar.email = this.validateForm.controls['email'].value;
			rar.password = this.validateForm.controls['password'].value;
			rar.re_password = this.validateForm.controls['checkPassword'].value;
			rar.first_name = this.validateForm.controls['first_name'].value;
      rar.last_name = this.validateForm.controls['last_name'].value;

			this.register(rar);
		}
	}

	updateConfirmValidator(): void {
		/** wait for refresh value */
		Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
	}

	confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
		return {};
	};


	ngOnInit(): void {
		this.validateForm = this.fb.group({
			email: [null, [Validators.email, Validators.required]],
			password: [null, [Validators.minLength(8), Validators.required]],
			checkPassword: [null, [Validators.required, this.confirmationValidator]],
			first_name: [null, [Validators.minLength(2), Validators.maxLength(20), Validators.required]],
      last_name: [null, [Validators.minLength(2), Validators.maxLength(20), Validators.required]],
		});
	}

	private register(rar: RegisterApiRequest) {
    // unsubscription is handled by AuthService
		this.authService.register(rar)
			.subscribe(
				registered => {
					if (registered) {
						this.login(rar.email, rar.password);
					} else {
						this.loading = false;
					}
				},
				error => {
					this.handleRegistrationError(error);
					this.loading = false;
				});
	}

	private login(email: string, password: string) {
		const lar = new LoginApiRequest(email, password);

    // unsubscription is handled by AuthService
		this.authService.login(lar)
			.subscribe(_ => {
				this.loading = false;
				this.router.navigate(['/', 'projects', 'my']);
				window.location.reload();
			});
	}

	dirtyOrHasErrors(label: string) {
		return this.validateForm.get(label).dirty && this.validateForm.get(label).errors
	}

	getFieldValidateStatus(field: string): string {
		if (this.dirtyOrHasErrors(field)) {
			return 'error';
		} else {
			return this.validateForm.get(field).valid ? 'success' : '';
		}
	}

	private handleRegistrationError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error(`An error occurred: ${error.error.message}`);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			for (let key in error.error) {
				const control = this.validateForm.controls[key];
				if (control) {
					control.setErrors({ 'customerror': error.error[key] });
				} else {
					console.warn(`field ${key} is not represented in front-end`);
				}
			}
		}
		// return an observable with a user-facing error message
		return throwError(
			'Something bad happened; please try again later.');
	}
}
