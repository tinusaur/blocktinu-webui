import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { SocialAuthButtonsComponent } from './social-auth-buttons/social-auth-buttons.component';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from '../shared/shared.module';
import { SocialAuthProviderComponent } from './social-auth-provider/social-auth-provider.component';
import { ConfirmPasswordResetComponent } from './confirm-password-reset/confirm-password-reset.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { NzFormModule, NzSpinModule, NzGridModule, NzInputModule, NzResultModule, NzModalModule, NzButtonModule, NzAlertModule, NzIconModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    SocialAuthButtonsComponent,
    SocialAuthProviderComponent,
    ActivateAccountComponent,
    ConfirmPasswordResetComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    NzGridModule,
    NzSpinModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzAlertModule,
    NzIconModule,
    NzResultModule,
    NzModalModule,
    SharedModule
  ]
})
export class AuthModule { }
