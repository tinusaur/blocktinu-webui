import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, CanLoad, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { User } from 'src/app/core/models/User.model';
import { filter, first, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private authService: AuthService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): Observable<boolean> {
    let url = `/${route.path}`;

    return this.checkLogin(url);
  }

  checkLogin(url: string): Observable<boolean> {
    const tokenInfo = this.authService.credentialsService.getTokenInfo();
    if (!tokenInfo || !tokenInfo.auth_token) {
      // store the attempted url for redirecting
      this.authService.redirectUrl = url;
      this.router.navigate(['/auth/login']);
      return of(false);
    }

    if (this.authService.credentialsService.currentUser) {
      return of(true);
    } else {
      return this.authService.credentialsService.userObservable.pipe(
        filter(v => v != null),
        first(),
        switchMap((user: User) => {
          return of(true);
        }));
    }

  }

}
