import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-social-auth-provider',
  templateUrl: './social-auth-provider.component.html',
  styleUrls: ['./social-auth-provider.component.scss']
})
export class SocialAuthProviderComponent implements OnInit {

  public loading: boolean = true;
  public error: boolean = false;

  // Social auth response
  // See https://djoser.readthedocs.io/en/latest/social_endpoints.html
  // See https://github.com/st4lk/django-rest-social-auth
  private code: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(queryParamMap => {
      this.code = queryParamMap.get('code');

      if (!this.code) {
        this.error = true;
        this.loading = false;

      } else {
        // login

        let provider: 'facebook' | 'google-oauth2' = 'facebook';
        const scope = queryParamMap.get('scope');
        if (scope && scope.includes('google')) {
          provider = 'google-oauth2';
        }


        // unsubscription is handled by AuthService
        this.authService.getTokenForSucceededSocialAuth(provider, this.code)
          .subscribe(success => {
            this.error = !success.token;
            this.router.navigate(['/', 'projects', 'migrate-demo']);
            this.loading = false;
          }, error => {
            this.loading = false;
            this.error = true;
          });
      }
    });
  }

}
