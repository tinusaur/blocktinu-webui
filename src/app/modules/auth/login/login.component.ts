import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { LoginApiRequest } from '../models/login-api-request.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    public userService: UserService,
    public router: Router) {
  }

  loading: boolean = false;
  validateForm: FormGroup;
  resetPasswordForm: FormGroup;

  resetPasswordRequestSent: boolean = false;
  forgotPasswordModal: boolean = false;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      const email = this.validateForm.controls['email'].value;
      const password = this.validateForm.controls['password'].value;
      this.login(email, password);
    }
  }

  submitResetPasswordForm(): void {
    for (const i in this.resetPasswordForm.controls) {
      this.resetPasswordForm.controls[i].markAsDirty();
      this.resetPasswordForm.controls[i].updateValueAndValidity();
    }

    if (this.resetPasswordForm.valid) {

      this.loading = true;
      const email = this.resetPasswordForm.controls['email'].value;
      // unsubscription is handled by AuthService
      this.authService.resetPasswordRequest(email)
        .subscribe(_v => {
          this.resetPasswordRequestSent = true;
          this.loading = false;
        }, error => {
          this.loading = false;
        });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.minLength(5), Validators.required]],
      other: { value: null, disabled: true }
    });

    this.resetPasswordForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
    });
  }

  private login(email: string, password: string) {
    this.loading = true;
    const lar = new LoginApiRequest(email, password);

    // unsubscription is handled by AuthService
    this.authService.login(lar)
      .subscribe(
        _ => {
          this.loading = false;
          this.router.navigate(['/', 'projects', 'migrate-demo']);
        },
        error => {
          for (let key in error.error) {
            if (key === 'non_field_errors') {
              const error_description = error.error[key][0];
              this.validateForm.controls['other'].setErrors({ 'invalid_grant': error_description })
              break;
            }
          }
          this.loading = false;
        });
  }

}
