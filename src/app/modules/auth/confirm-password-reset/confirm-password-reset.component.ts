import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-confirm-password-reset',
  templateUrl: './confirm-password-reset.component.html',
  styleUrls: ['./confirm-password-reset.component.scss']
})
export class ConfirmPasswordResetComponent implements OnInit {
  uid: string;
  token: string;
  loading: boolean = false;

  v: any;

  validateForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
    private nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
    });

    // unsubscription is handled by ActivatedRoute
    this.route.paramMap.subscribe(paramMap => {
      this.uid = paramMap.get('uid');
      this.token = paramMap.get('token');
    })
  }


  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.invalid) {
      return;
    }
    this.loading = true;

    const new_password = this.validateForm.controls['password'].value;
    const re_new_password = this.validateForm.controls['checkPassword'].value;
    // unsubscription is handled by AuthService
    this.authService.confirmPasswordReset(this.uid, this.token, new_password, re_new_password)
      .subscribe(v => {
        this.loading = false;
        this.nzMessageService.success("Successfully updated password.");
        this.router.navigate(['/', 'auth', 'login']);
      },
        error => {
          this.loading = false;
        });
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };


}

