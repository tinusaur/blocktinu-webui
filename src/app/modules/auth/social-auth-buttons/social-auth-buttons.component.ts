import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-social-auth-buttons',
  templateUrl: './social-auth-buttons.component.html',
  styleUrls: ['./social-auth-buttons.component.scss']
})
export class SocialAuthButtonsComponent implements OnInit {
  constructor(public authService: AuthService) { }

  ngOnInit() {
  }


  onGoogleButtonClick() {
    // unsubscription is handled by AuthService
    this.authService.loginUsingGoogle().subscribe();
  }

  onFacebookButtonClick() {
    // unsubscription is handled by AuthService
    this.authService.loginUsingFacebook().subscribe();
  }

}
