import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, CanLoad, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { UserRole, User } from 'src/app/core/models/User.model';
import { first, switchMap, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StaffGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private authService: AuthService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    let url: string = state.url;

    return this.check(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): Observable<boolean> {
    let url = `/${route.path}`;

    return this.check(url);
  }

  check(url: string): Observable<boolean> {
    if (this.authService.credentialsService.currentUser) {
      return this.getResultOrRedirect();
    } else {
      return this.authService.credentialsService.userObservable.pipe(
        filter(v => v != null),
        first(),
        switchMap((user: User) => this.getResultOrRedirect()));
    }
  }

  private getResultOrRedirect() {
    const result = this.authService.credentialsService.currentUser.role == UserRole.STAFF;
    if (!result) this.router.navigate(['/', 'projects', 'my']);
    return of(result);

  }
}
