import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {
  private uid: string;
  private token: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {

    // unsubscription is handled by ActivatedRoute
    this.route.paramMap.subscribe(paramMap => {
      this.uid = paramMap.get('uid');
      this.token = paramMap.get('token');
      if (!this.uid || !this.token) {
        return;
      }

      // unsubscription is handled by AuthService
      this.authService.activate(this.uid, this.token).subscribe(v => {
        this.router.navigate(['/', 'projects', 'my']);
      })
    })
  }

}
