import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UsersRouting } from './users-routing.module';
import { NzListModule, NzPaginationModule, NzCardModule, NzGridModule, NzTabsModule, NzDescriptionsModule, NzTypographyModule, NzResultModule, NzAvatarModule, NzButtonModule, NzBadgeModule, NzDividerModule, NzSelectModule, NzInputModule } from 'ng-zorro-antd';
import { ProjectsModule } from '../projects/projects.module';
import { ClassroomsModule } from '../classrooms/classrooms.module';
import { NotificationsPageComponent } from './components/notifications-page/notifications-page.component';
import { FormsModule } from '@angular/forms';
import { UsersFiltersComponent } from './components/users-filters/users-filters.component';

@NgModule({
  imports: [
    CommonModule,
    UsersRouting,
    NzListModule,
    NzPaginationModule,
    NzGridModule,
    NzCardModule,
    NzTabsModule,
    NzDescriptionsModule,
    NzTypographyModule,
    NzResultModule,
    NzAvatarModule,
    NzButtonModule,
    NzInputModule,
    NzBadgeModule,
    NzDividerModule,
    FormsModule,
    NzSelectModule,
    ProjectsModule,
    ClassroomsModule
  ],
  declarations: [
    UsersListComponent,
    UsersFiltersComponent,
    UsersDetailComponent,
    NotificationsPageComponent
  ]
})
export class UsersModule { }
