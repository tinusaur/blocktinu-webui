import { Injectable } from '@angular/core';
import { Notification } from 'src/app/core/models/Notification.models';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { CredentialsService } from '../../auth/credentials.service';
import { Observable, of, EMPTY, throwError } from 'rxjs';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsResolverService implements Resolve<PaginatedApiResponse<Notification>> {

    constructor(
      private notificationsService: NotificationsService,
      private credentialsService: CredentialsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
      Observable<PaginatedApiResponse<Notification>> | Observable<never> {

      let recipientId = null;
      const page = route.queryParamMap.get('page');
      const page_size = route.queryParamMap.get('page_size');

      if ('recipient_id_in_parent_parameter' in route.data && route.data.recipient_id_in_parent_parameter) {
        // e.g. /users/:userId/notifications
        recipientId = route.parent.paramMap.get(route.data.recipient_id_in_parent_parameter);
      }


      let filters = [];

      if (recipientId) {
        filters.push({ param: 'recipient_id', value: recipientId });
      }
     if (page) {
        filters.push({ param: 'page', value: page });
      }

      if (page_size) {
        filters.push({ param: 'page_size', value: page_size });
      }

      return this.notificationsService.getByFilters(filters).pipe(
        take(1),
        mergeMap((classrooms: PaginatedApiResponse<Notification>) => {
          if (classrooms) {
            return of(classrooms);
          } else {
            return EMPTY;
          }
        }),
        catchError(e => {
          if (e instanceof HttpErrorResponse) {
            if (e.status == 404) {
              return EMPTY;
            }
          }
          throwError(e); // TODO
        })
      );
    }

}
