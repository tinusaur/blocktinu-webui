/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserDetailsResolverService } from './user-details-resolver.service';

describe('Service: UserDetailsResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserDetailsResolverService]
    });
  });

  it('should ...', inject([UserDetailsResolverService], (service: UserDetailsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
