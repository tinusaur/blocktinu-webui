import { Injectable } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, of, EMPTY, throwError } from 'rxjs';
import { User } from 'src/app/core/models/User.model';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserDetailsResolverService implements Resolve<User>{

  constructor(private userService: UserService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<User> | Observable<never> {

    const id = route.paramMap.get('userId');

    return this.userService.getUserById(id).pipe(
      take(1),
      mergeMap((user: User) => {
        if (user) {
          return of(user);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
