/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotificationsResolverService } from './notifications-resolver.service';

describe('Service: NotificationsResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationsResolverService]
    });
  });

  it('should ...', inject([NotificationsResolverService], (service: NotificationsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
