import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from 'src/app/core/models/User.model';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { UserService } from 'src/app/core/services/user.service';
import { Observable, of, EMPTY, throwError } from 'rxjs';
import { take, mergeMap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersResolverService implements Resolve<PaginatedApiResponse<User>> {

  constructor(
    private userService: UserService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<PaginatedApiResponse<User>> | Observable<never> {

    const query = route.queryParamMap.get('q');
    const page = route.queryParamMap.get('page');
    const page_size = route.queryParamMap.get('page_size');

    const filters = [];

    if (query) {
      filters.push({ param: 'search', value: query });
    }

    if (page) {
      filters.push({ param: 'page', value: page });
    }

    if (page_size) {
      filters.push({ param: 'page_size', value: page_size });
    }

    return this.userService.getByFilters(filters).pipe(
      take(1),
      mergeMap((users: PaginatedApiResponse<User>) => {
        if (users) {
          return of(users);
        } else {
          return EMPTY;
        }
      }),
      catchError(e => {
        if (e instanceof HttpErrorResponse) {
          if (e.status == 404) {
            return EMPTY;
          }
        }
        throwError(e); // TODO
      })
    );
  }
}
