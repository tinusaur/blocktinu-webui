import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, UserRole } from 'src/app/core/models/User.model';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit {
  user: User;
  readonly: boolean = true;
  loading: boolean = false;
  static TAB_INDEXES = {
    projects: 0,
    classrooms: 1,
    notifications: 2
  };

  selectedTabsIndex: number = 0;

  constructor(
    public credentialsService: CredentialsService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.data.subscribe((data: { user: User }) => {
      this.user = data.user;
      if (this.user && this.credentialsService.currentUser && (this.credentialsService.currentUser.role == UserRole.STAFF || this.credentialsService.currentUser.id === this.user.id)) {
        this.readonly = false;
      }
      this.loading = false;

    });

    this.route.url.subscribe(u => {
      const tab = this.route.snapshot.firstChild.data['tab'];
      this.selectedTabsIndex = UsersDetailComponent.TAB_INDEXES[tab];
    });
  }

  changeTab(tab: string): void {
    this.router.navigate([tab], { relativeTo: this.route })
  }

  patchUser(patchObj: any): void {
    this.loading = true;
    this.userService.patchUser(this.user.id, patchObj)
      .subscribe(user => {
        this.user = user;
        this.loading = false;
      });
  }
}
