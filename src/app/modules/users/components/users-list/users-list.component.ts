import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { User } from 'src/app/core/models/User.model';
import { PaginatedApiResponse } from 'src/app/core/models/api-response/paginated-api-response';
import { TargetDeviceService } from 'src/app/core/services/target-device.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  users: PaginatedApiResponse<User>;
  loading: boolean = true;
  page = 1;
  page_size = 20;

  constructor(
    public tds: TargetDeviceService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(params => {
      this.page_size = +params.get('page_size') || this.page_size;
      this.page = +params.get('page') || this.page;
    });

    // unsubscription is handled by ActivatedRoute
    this.route.data.subscribe((data: { users: PaginatedApiResponse<User> }) => {
      this.users = data.users;
      this.loading = false;
    });
  }


  onPageChange(pi: number): void {
    this.loading = true;
    this.page = pi;
    const queryParams: Params = { page: pi, page_size: this.page_size };
    this.updateQueryParams(queryParams);
  }

  private updateQueryParams(queryParams: Params) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
        queryParamsHandling: "merge", // remove to replace all query params by provided
      });
  }

  search(q: string) {
    this.loading = true;
    const queryParams: Params = { q };
    this.updateQueryParams(queryParams);
  }

}
