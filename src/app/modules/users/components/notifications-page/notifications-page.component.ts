import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Notification } from 'src/app/core/models/Notification.models';
import { CredentialsService } from 'src/app/modules/auth/credentials.service';
import { RealtimeNotificationsService } from 'src/app/core/services/socket/realtime-notifications.service';

@Component({
  selector: 'app-notifications-page',
  templateUrl: './notifications-page.component.html',
  styleUrls: ['./notifications-page.component.scss']
})
export class NotificationsPageComponent implements OnInit {
  loading = false;
  page = 1;
  page_size = 20;

  constructor(
    public credentialsService: CredentialsService,
    public rns: RealtimeNotificationsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    // unsubscription is handled by ActivatedRoute
    this.route.queryParamMap.subscribe(params => {
      this.page_size = +params.get('page_size') || this.page_size;
      this.page = +params.get('page') || this.page;
    });

  }

  markAllAsRead() {
    this.rns.markAllAsRead();
  }

  removeNotification(notification: Notification) {
    this.rns.removeNotification(notification);
  }

  onPageChange(pi: number): void {
    this.rns.loadNotificationForPage(pi);
  }
}
