import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UserDetailsResolverService } from './services/user-details-resolver.service';
import { UsersResolverService } from './services/users-resolver.service';
import { ProjectsListPageComponent } from '../projects/components/projects-list-page/projects-list-page.component';
import { ProjectsResolverService } from '../projects/services/projects-resolver.service';
import { ProjectsExploreComponent } from '../projects/components/projects-explore/projects-explore.component';
import { ClassroomsComponent } from '../classrooms/components/classrooms/classrooms.component';
import { ClassroomsResolverService } from '../classrooms/services/classrooms-resolver.service';
import { NotificationsResolverService } from './services/notifications-resolver.service';
import { NotificationsPageComponent } from './components/notifications-page/notifications-page.component';
import { AuthGuard } from '../auth/auth.guard';
import { StaffGuard } from '../auth/staff.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: ':userId',
        component: UsersDetailComponent,
        resolve: {
          user: UserDetailsResolverService,
        },
        runGuardsAndResolvers: 'pathParamsChange',
        children: [
          {
            path: 'projects',
            component: ProjectsExploreComponent,
            resolve: {
              projects: ProjectsResolverService,
            },
            data: {
              tab: 'projects',
              only_current_user_projects: false,
              owner_id_parent_parameter: 'userId',
              hide_trending: true
            },
            runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
          },
          {
            path: 'classrooms',
            component: ClassroomsComponent,
            resolve: {
              classrooms: ClassroomsResolverService,
            },
            data: {
              tab: 'classrooms',
              // hide_create_button: true,
              only_current_user_classrooms: false,
              id_in_members_in_parent_parameter: 'userId'
            },
            runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
          },
          {
            path: 'notifications',
            component: NotificationsPageComponent,
            // resolve: {
              // notifications: NotificationsResolverService,
            // },
            data: {
              tab: 'notifications',
              // recipient_id_in_parent_parameter: 'userId'
            },
            runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
          },
          {
						path: '',
						pathMatch: 'full',
						redirectTo: 'projects'
					}
        ]
      },
      {
        path: '',
        component: UsersListComponent,
        resolve: {
          users: UsersResolverService
        },
        canActivate: [AuthGuard, StaffGuard],
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRouting { }
