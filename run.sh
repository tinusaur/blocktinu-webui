#!/bin/bash

echo webui env: $ENV

npm install

if [ "$ENV" = "production" ]; then
  # Build and exit container (use your server to host files)
  ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer --extract-css --base-href=$WEBCLIENT_BASE_HREF
else
  # run in dev mode (use proxypass)
  ng serve --host 0.0.0.0 --port $WEBCLIENT_PUBLIC_PORT --base-href=$WEBCLIENT_BASE_HREF --public-host=$WEBCLIENT_PUBLIC_HOST_WITH_PORT --disable-host-check --disableHostCheck
fi
